package ru.nsu.fit.cherniak.lab5.Client;

import ru.nsu.fit.cherniak.lab5.Server.parse.HttpParser;
import ru.nsu.fit.cherniak.lab5.Server.stuff.HttpSender;

public interface HttpClienter {
    void request(HttpSender httpSender);

    void response(HttpParser httpParser);
}
