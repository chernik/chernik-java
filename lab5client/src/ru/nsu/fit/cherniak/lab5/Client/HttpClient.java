package ru.nsu.fit.cherniak.lab5.Client;

import ru.nsu.fit.cherniak.lab5.Server.exc.ExceptionHttpSender;
import ru.nsu.fit.cherniak.lab5.Server.parse.HttpParser;
import ru.nsu.fit.cherniak.lab5.Server.stuff.HttpSender;
import ru.nsu.fit.cherniak.lab5.Server.stuff.LogThread;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.*;
import java.util.concurrent.Callable;

public class HttpClient extends LogThread {
    private String name;
    private int port;
    private Runnable onError;

    public HttpClient() {
        name = "localhost";
        port = 8080;
        onError = () -> {};
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    private boolean send(HttpClienter httpClienter) {
        setState("Startup...");
        InetAddress inetAddress;
        try {
            inetAddress = InetAddress.getByName(name);
        } catch (UnknownHostException e) {
            setError("Cannot get address");
            return true;
        }
        Socket socket = new Socket();
        setState("Opening " + name + ":" + port + "...");
        try {
            //socket = new Socket(inetAddress, port);
            socket.connect(new InetSocketAddress(inetAddress, port), 1000);
        } catch (IOException e) {
            setError("Cannot open socket");
            return true;
        }
        if(!socket.isConnected()){
            setError("Timeout");
            return true;
        }
        try {
            socket.setSoTimeout(1000);
        } catch (SocketException e) {
            setError("Cannot set timeout");
            return true;
        }
        OutputStream os;
        BufferedInputStream is;
        try {
            os = socket.getOutputStream();
            is = new BufferedInputStream(socket.getInputStream());
        } catch (IOException e) {
            setError("Cannot open stream");
            return true;
        }
        HttpSender httpSender = new HttpSender(os, HttpSender.CLIENToutputMode);
        HttpParser httpParser = new HttpParser(is);
        httpParser.setInputMode(HttpParser.CLIENTinputMode);
        setState("Open host: " + inetAddress.getHostAddress() + ". Handling...");
        httpClienter.request(httpSender);
        try {
            httpSender.goAway();
            //os.write("GET / HTTP/1.0\r\n\r\n".getBytes());
            os.flush();
        } catch (ExceptionHttpSender | IOException exceptionHttpSender) {
            setError("Cannot send data");
            return true;
        }
        httpClienter.response(httpParser);
        try {
            socket.close();
        } catch (IOException e) {
            setError("Cannot close socket");
            return true;
        }
        return false;
    }

    public void start(HttpClienter httpClienter) {
        Callable<Object> c = this.getThread(() -> {
            if(send(httpClienter)){
                onError.run();
            }
        });
        Runnable r = () -> {
            try {
                c.call();
            } catch (Exception e) {
                e.printStackTrace();
            }
        };
        (new Thread(r)).start();
    }

    public void setEventError(Runnable r){
        onError = r;
    }
}
