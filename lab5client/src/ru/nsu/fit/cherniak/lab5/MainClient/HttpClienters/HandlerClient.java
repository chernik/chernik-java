package ru.nsu.fit.cherniak.lab5.MainClient.HttpClienters;

import ru.nsu.fit.cherniak.lab5.Client.HttpClienter;
import ru.nsu.fit.cherniak.lab5.Server.exc.ExceptionHttpParser;
import ru.nsu.fit.cherniak.lab5.Server.parse.HttpParser;
import ru.nsu.fit.cherniak.lab5.Server.stuff.HttpSender;

/**
 * Врарппер для клиента
 */
public class HandlerClient implements HttpClienter {
    private HttpClienter httpClienter;
    private String action;

    public HandlerClient(String action, HttpClienter httpClienter) {
        this.httpClienter = httpClienter;
        this.action = action;
    }

    @Override
    public void request(HttpSender httpSender) {
        //httpSender.setVersion("HTTP/1.0");
        httpSender.setVar("LINK", "/admin" +
                (action.equals("login") ? "" : "/" + action));
        httpClienter.request(httpSender);
        httpSender.sendData("");
    }

    @Override
    public void response(HttpParser httpParser) {
        /*try {
            System.out.println(
                    "BODY RESPONSE:\n" +  new String(httpParser.getBody()) + "\n------"
            );
        } catch (ExceptionHttpParser exceptionHttpParser) {
            System.out.println("ERROR");
        }*/
        httpClienter.response(httpParser);
    }
}
