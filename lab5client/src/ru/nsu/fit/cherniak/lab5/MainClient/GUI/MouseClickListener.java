package ru.nsu.fit.cherniak.lab5.MainClient.GUI;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Для работы только с одним событием
 */
public interface MouseClickListener extends MouseListener {

    @Override
    default void mousePressed(MouseEvent e) {

    }

    @Override
    default void mouseReleased(MouseEvent e) {

    }

    @Override
    default void mouseEntered(MouseEvent e) {

    }

    @Override
    default void mouseExited(MouseEvent e) {

    }
}
