package ru.nsu.fit.cherniak.lab5.MainClient;

import ru.nsu.fit.cherniak.lab5.ThreadPool.Syncro;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class ClientTerminal {
    private ClientContext clientContext;
    private Map<String, String> params;
    private PrintStream os;
    private Scanner is;
    private String backResult;
    private Syncro o;

    public ClientTerminal(InputStream is, OutputStream os) {
        this(new ClientContext(), is, os);
    }

    public ClientTerminal(ClientContext clientContext, InputStream is, OutputStream os) {
        o = new Syncro();
        this.clientContext = clientContext;
        this.params = new HashMap<>();
        this.is = new Scanner(is);
        this.is.useDelimiter("[\\r\\n]+");
        if (os instanceof PrintStream) {
            this.os = (PrintStream) os;
        } else {
            this.os = new PrintStream(os);
        }
        clientContext.setEventError(() -> {
            backResult = clientContext.getState();
            o.notifyS();
        });
    }

    private void saveResult(String res) {
        backResult = res;
        o.notifyS();
    }

    public void addHostPort(String host, String port) {
        params.put("host", host);
        params.put("port", port);
    }

    public boolean inputParam(String key, String prompt) {
        if (params.containsKey(key)) {
            return false;
        }
        os.print(prompt);
        String res = is.next();
        if (res.equals("")) {
            return true;
        }
        params.put(key, res);
        return true;
    }

    public void login() {
        while (true) {
            if (!(inputParam("user", "User name > ") ||
                    inputParam("pass", "Password > ") ||
                    inputParam("host", "Host name > ") ||
                    inputParam("port", "Port > "))) break;
        }
    }

    private boolean goNextFunc() {
        os.print("> ");
        String[] com = is.next().split("[ ]+");
        if (com.length == 0) {
            return false;
        }
        switch (com[0]) {
            case "logout":
                clientContext.logout(this::saveResult);
                o.waitS();
                if (!backResult.equals("sucs")) {
                    os.println("Someth wrong > " + backResult);
                    os.print("Do you want to logout without close this session?");
                    return is.next().equals("y");
                }
                os.println("You logouted!");
                return true;
            case "action":
                if (com.length < 2) {
                    os.println("Put the action!");
                    return false;
                }
                String action = com[1];
                String par;
                if (com.length < 3) {
                    par = "~~~";
                } else {
                    par = com[2];
                }
                clientContext.action(action, par, this::saveResult);
                o.waitS();
                os.println("Result:");
                os.println("----------------");
                os.println(backResult.replace("<br>", "\n"));
                os.println("----------------");
                return false;
            default:
                os.println("Wrong command!");
                return false;
        }
    }

    public void start() {
        while (true) {
            login();
            try {
                clientContext.login(
                        params.get("host"),
                        Integer.valueOf(params.get("port")),
                        params.get("user"),
                        params.get("pass"),
                        this::saveResult
                );
                o.waitS();
            } catch (NumberFormatException e) {
                backResult = "Port is not a number";
            }
            if (!backResult.equals("sucs")) {
                os.println("Unsuccessful > " + backResult);
                os.print("Repeat? y/n > ");
                String res = is.next();
                if (res.equals("y")) {
                    continue;
                } else {
                    break;
                }
            }
            os.println("You logged!");
            while (true) {
                if (goNextFunc()) break;
            }
            os.print("Quit? y/n > ");
            String res = is.next();
            if (res.equals("y")) {
                break;
            }
            params.clear();
        }
    }
}
