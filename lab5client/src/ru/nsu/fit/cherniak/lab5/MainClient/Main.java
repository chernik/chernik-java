package ru.nsu.fit.cherniak.lab5.MainClient;

import ru.nsu.fit.cherniak.lab5.MainClient.GUI.GUIFrame;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        if(args.length == 0){
            javax.swing.SwingUtilities.invokeLater(GUIFrame::new);
            return;
        }
        String[] user = args[0].split(":");
        ClientTerminal term = new ClientTerminal(
                System.in,
                System.out
        );
        term.addHostPort(user[0], user[1]);
        term.start();
    }
}
