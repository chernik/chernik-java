package ru.nsu.fit.cherniak.lab5.MainClient.GUI;

import ru.nsu.fit.cherniak.lab5.MainClient.ClientContext;
import ru.nsu.fit.cherniak.lab5.Stuff;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Гуишное окно, работает с ClientContext
 */
public class GUIFrame extends JFrame {
    private ClientContext clientContext;
    private Map<String, JButton> buttons;
    private Map<String, JTextField> fields;
    private JLabel log;
    private JPanel loginPanel;
    private JPanel functionalPanel;
    private Timer timer;
    private JComboBox<String> actions;
    private JLabel actionResult;
    private JScrollPane scrollActionResult;
    private int windowState;
    private static int LOGIN = 1;
    private static int FUNC = 2;

    public GUIFrame() {
        // Global
        Font f = new Font("Courier New", Font.PLAIN, 18); // Шрифт
        clientContext = new ClientContext(); // Драйвер
        buttons = new HashMap<>(); // Кнопки
        log = new JLabel(); // Лог
        log.setPreferredSize(new Dimension(0, 20)); // Высота 20
        log.setFont(new Font("Arial", Font.ITALIC, 12));
        loginPanel = new JPanel(); // Панель входа
        functionalPanel = new JPanel(); // Панель функций
        fields = new HashMap<>(); // Текстовые поля
        // Frame
        setDefaultLookAndFeelDecorated(true);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        addWindowListener((WindowCloseListener)this::onWindowClosing);
        setPreferredSize(new Dimension(400, 500)); // Размеры окна
        setVisible(true);
        // Fields
        fields.put("host", new JTextField("localhost"));
        fields.put("port", new JTextField("8080"));
        fields.put("user", new JTextField("admin"));
        fields.put("pass", new JPasswordField("12345"));
        fields.put("action", new JTextField("logout"));
        // Others
        ((JPasswordField) fields.get("pass")).setEchoChar('~');
        fields.forEach((key, value) -> {
            value.setFont(f);
            value.setPreferredSize(new Dimension(0, 30));
        });
        actions = new JComboBox<>(clientContext.getActions());
        actions.setPreferredSize(new Dimension(0, 30));
        actionResult = new JLabel();
        actionResult.setVerticalAlignment(JLabel.NORTH);
        scrollActionResult = new JScrollPane(actionResult);
        scrollActionResult.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollActionResult.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrollActionResult.setPreferredSize(new Dimension(0, 300));
        // Buttons
        buttons.put("login", new JButton("GO ON"));
        buttons.put("action", new JButton("ACTION"));
        buttons.put("logout", new JButton("LOGOUT"));
        buttons.forEach((key, value) -> {
            value.setFont(f);
            value.setPreferredSize(new Dimension(0, 30));
        });
        // Layout
        loginPanel.setPreferredSize(new Dimension(400, 300));
        loginPanel.setLayout(new GridBagLayout());
        functionalPanel.setPreferredSize(new Dimension(400, 500));
        functionalPanel.setLayout(new GridBagLayout());
        getContentPane().add(loginPanel, 0); // Что-то должно лежать
        // Listeners
        focusNext("host", "port");
        focusNext("port", "user");
        focusNext("user", "pass");
        fields.get("pass").addActionListener(this::buttonLoginClick);
        buttons.get("login").addActionListener(this::buttonLoginClick);
        actions.addActionListener(this::buttonActionClick);
        buttons.get("action").addActionListener(this::buttonActionClick);
        buttons.get("logout").addActionListener(this::buttonLogoutClick);
        // Timer
        timer = new Timer(10, this::onTimer);
        // Go
        setLoginFunctionalPanelComps();
        setLoginFrameComps();
        setLocationRelativeTo(null);
    }

    private void onWindowClosing(WindowEvent windowEvent) {
        if(windowState == LOGIN || 0 == JOptionPane.showConfirmDialog(
                this,
                "Do you really want to exit?")){
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        }
    }

    synchronized private void logging(String log){
        SwingUtilities.invokeLater(() -> {
            this.log.setText(log);
        });
    }

    private void buttonLoginClick(ActionEvent actionEvent) {
        timer.start();
        clientContext.login(
                fields.get("host").getText(),
                Integer.valueOf(fields.get("port").getText()),
                fields.get("user").getText(),
                new String(((JPasswordField) fields.get("pass")).getPassword()),
                res -> {
                    timer.stop();
                    if (!res.equals("sucs")) {
                        logging(res);
                        return;
                    }
                    SwingUtilities.invokeLater(this::setFunctionalPanelComps);
                }
        );
    }

    private void buttonLogoutClick(ActionEvent actionEvent) {
        timer.start();
        clientContext.logout(
                res -> {
                    timer.stop();
                    if (!res.equals("sucs")) {
                        logging(res);
                        if(JOptionPane.YES_OPTION != JOptionPane.showConfirmDialog(
                                this,
                                "Someth goes wrong.\nDo you want to logout without close this session?")){
                            return;
                        }
                    }
                    SwingUtilities.invokeLater(this::setLoginFrameComps);
                }
        );
    }

    private String getPar(String action){
        String par;
        switch(action){
            case "changePass":
                par = JOptionPane.showInputDialog(this, "Enter the password");
                if(par == null){
                    return null;
                }
                try {
                    par = Stuff.getMD5(par);
                } catch (NoSuchAlgorithmException e1) {
                    logging("Cannot find MD5");
                }
                break;
            case "registerPlugin":
                par = JOptionPane.showInputDialog(this, "Enter the name of uploaded file\nFor help put 'HELP'", "HELP");
                if(par == null){
                    return null;
                }
                break;
            default:
                par = "~~~";
        }
        return par;
    }

    private void buttonActionClick(ActionEvent actionEvent){
        String action = (String) actions.getSelectedItem();
        if(action == null){
            return;
        }
        String par = getPar(action);
        if(par == null){
            return;
        }
        timer.start();
        clientContext.action(
                action,
                par,
                res -> {
                    if(res == null){
                        logging("Cannot get result");
                        return;
                    }
                    logging("Sucs");
                    SwingUtilities.invokeLater(() -> {
                        actionResult.setText(res);
                    });
                }
        );
    }

    private void onTimer(ActionEvent actionEvent) {
        logging(clientContext.getState() + (clientContext.hasError() ? "$$" : ""));
    }

    private void setLoginFunctionalPanelComps() {
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.ipadx = 200;
        loginPanel.add(fields.get("host"), c);
        c.gridx = 1;
        c.ipadx = 100;
        loginPanel.add(fields.get("port"), c);
        c.gridx = 0;
        c.gridy = 1;
        c.gridwidth = 2;
        c.ipadx = 300;
        loginPanel.add(fields.get("user"), c);
        c.gridy = 2;
        loginPanel.add(fields.get("pass"), c);
        c.gridy = 3;
        loginPanel.add(buttons.get("login"), c);
        c.gridy = 0;
        functionalPanel.add(actions, c);
        c.gridy = 1;
        functionalPanel.add(buttons.get("action"), c);
        c.gridy = 2;
        functionalPanel.add(scrollActionResult, c);
        c.gridy = 3;
        functionalPanel.add(buttons.get("logout"), c);
    }

    private void setLoginFrameComps() {
        windowState = LOGIN;
        setPreferredSize(loginPanel.getPreferredSize());
        setMinimumSize(loginPanel.getPreferredSize());
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridwidth = 2;
        c.ipadx = 300;
        c.gridy = 4;
        loginPanel.add(log, c);
        logging("");
        getContentPane().removeAll();
        getContentPane().add(loginPanel, 0);
        loginPanel.validate();
        loginPanel.repaint();
        pack();
    }

    private void setFunctionalPanelComps() {
        windowState = FUNC;
        setPreferredSize(functionalPanel.getPreferredSize());
        setMinimumSize(functionalPanel.getPreferredSize());
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.ipadx = 300;
        c.gridy = 4;
        functionalPanel.add(log, c);
        logging("");
        getContentPane().removeAll();
        getContentPane().add(functionalPanel, 0);
        functionalPanel.validate();
        functionalPanel.repaint();
        pack();
    }

    private void focusNext(String a, String b){
        fields.get(a).addActionListener(e -> {
            fields.get(a).select(0,0);
            fields.get(b).selectAll();
            fields.get(b).grabFocus();
        });
    }
}
