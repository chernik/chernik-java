package ru.nsu.fit.cherniak.lab5.MainClient;

import ru.nsu.fit.cherniak.lab5.Client.HttpClient;
import ru.nsu.fit.cherniak.lab5.Client.HttpClienter;
import ru.nsu.fit.cherniak.lab5.MainClient.HttpClienters.CBResult;
import ru.nsu.fit.cherniak.lab5.MainClient.HttpClienters.HandlerClient;
import ru.nsu.fit.cherniak.lab5.Server.exc.ExceptionHttpParser;
import ru.nsu.fit.cherniak.lab5.Server.parse.HttpParser;
import ru.nsu.fit.cherniak.lab5.Server.stuff.HttpSender;
import ru.nsu.fit.cherniak.lab5.Stuff;

import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

/**
 * Драйвер HttpClient для GUIFrame
 */
public class ClientContext {
    private String hash; // Сессия
    private HttpClient httpClient;

    public ClientContext() {
        hash = "";
        httpClient = new HttpClient();
    }

    /**
     * Для проверки состояния
     *
     * @return
     */
    public String getState() {
        return httpClient.getState();
    }

    public boolean hasError() {
        return httpClient.hasError();
    }

    public boolean isEnded() {
        return httpClient.isEnded();
    }

    public String[] getActions(){
        return new String[]{
                "ping",
                "changePass",
                "metrics",
                "restart",
                "registerPlugin",
                "getListPlugins"
        };
    }

    private String getHash(String str) {
        try {
            return Stuff.getMD5(str);
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }

    /**
     * Зайти на сервер
     *
     * @param host     Имя хоста
     * @param port     Порт
     * @param name     Имя пользователя
     * @param pass     Пароль
     * @param cbResult Калбэк для гуи
     */
    public void login(String host, int port, String name, String pass, CBResult cbResult) {
        String user = getHash(pass) + " " + name;
        httpClient.setName(host);
        httpClient.setPort(port);
        httpClient.start(new HandlerClient("login", new HttpClienter() {

            @Override
            public void request(HttpSender httpSender) {
                httpSender.setVar("user", user);
            }

            @Override
            public void response(HttpParser httpParser) {
                try {
                    hash = httpParser.getValue("hash");
                } catch (ExceptionHttpParser exceptionHttpParser) {
                    cbResult.accept("Error in get hash");
                    return;
                }
                if (hash.equals("")) {
                    byte[] body;
                    cbResult.accept("Cannot get hash");
                    return;
                }
                cbResult.accept("sucs");
            }
        }));
    }

    public void logout(CBResult result) {
        httpClient.start(new HandlerClient("logout", new HttpClienter() {
            @Override
            public void request(HttpSender httpSender) {
                httpSender.setVar("hash", hash);
            }

            @Override
            public void response(HttpParser httpParser) {
                byte[] body;
                try {
                    body = httpParser.getBody();
                } catch (ExceptionHttpParser exceptionHttpParser) {
                    result.accept("Cannot get result");
                    return;
                }
                if (!new String(body).equals("Logouted")) {
                    result.accept("Someth wrong");
                    return;
                }
                result.accept("sucs");
            }
        }));
    }

    public void action(String action, String par, CBResult result) {
        httpClient.start(new HandlerClient(action, new HttpClienter() {
            @Override
            public void request(HttpSender httpSender) {
                httpSender.setVar("hash", hash);
                httpSender.setVar("par", par);
            }

            @Override
            public void response(HttpParser httpParser) {
                byte[] body;
                try {
                    body = httpParser.getBody();
                } catch (ExceptionHttpParser exceptionHttpParser) {
                    result.accept(null);
                    return;
                }
                result.accept(new String(body));
            }
        }));
    }

    public void setEventError(Runnable r){
        httpClient.setEventError(r);
    }
}
