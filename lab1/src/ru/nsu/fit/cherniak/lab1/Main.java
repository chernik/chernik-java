package ru.nsu.fit.cherniak.lab1;

import ru.nsu.fit.cherniak.lab1.stuff.builders.DataPacket;
import ru.nsu.fit.cherniak.lab1.stuff.exc.ArgumentNotFoundException;
import ru.nsu.fit.cherniak.lab1.stuff.exc.ParserException;

public class Main {

    public static final String STRING_IS = "String is >\n";

    public static void main(String[] args) {
        String tempStr = "";
        if (args.length == 0) {
            tempStr = "%a% b\\%c %!if a!% d %!endif!% e %!repeat a!% f %!endrepeat!% g";
        }
        System.out.println(STRING_IS + tempStr);
        Template temp;
        try {
            temp = new Template(tempStr);
        } catch (ParserException e) {
            System.err.println("Parser error");
            return;
        }
        DataPacket data = new DataPacket();
        String res;
        // Test 1
        data.conds.put("a", true);
        data.iters.put("a", 5);
        data.values.put("a", "test1");
        System.out.println("Test 1 >");
        try {
            res = temp.fill(data);
        } catch (ArgumentNotFoundException e) {
            System.err.println("Fill error");
            return;
        }
        System.out.println(res);
        // Test 2
        data.conds.replace("a", false);
        data.iters.replace("a", 7);
        data.values.replace("a", "test2");
        System.out.println("Test 2 >");
        try {
            res = temp.fill(data);
        } catch (ArgumentNotFoundException e) {
            System.err.println("Fill error");
            return;
        }
        System.out.println(res);
        System.out.println("Sucs!\nThe end");
    }
}
