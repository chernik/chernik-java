package ru.nsu.fit.cherniak.lab1;

import ru.nsu.fit.cherniak.lab1.stuff.builders.*;
import ru.nsu.fit.cherniak.lab1.stuff.exc.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Template {

    private StrBaseBuilder[] arr;

    Template(String s) throws ParserException {
        List<StrBaseBuilder> build = new ArrayList<>();
        String[] arr = s.split("%");
        String buf = "";
        int state = 0;
        /*
        0 - simple
        1 - check
        2 - if data
        3 - check if data
        4 - for data
        5 - check for data
         */
        for (int i = 0; i < arr.length; i++) {
            switch (state) {
                case 0: // Обычный текст
                    if (arr[i].length() > 0 && arr[i].substring(arr[i].length() - 1).equals("\\")) { // Только для обычных строк
                        arr[i] = arr[i].substring(0, arr[i].length() - 1) + "%"; // Вставляем собстно процент без слэша
                    } else {
                        state = 1; // Следующий - проверяем
                    }
                    build.add(new StrDataBuilder(arr[i]));
                    break;
                case 1:
                    if (arr[i].substring(0, 1).equals("!")) { // Не проверяется, кончается ли на "!"
                        buf = arr[i].substring(1, arr[i].length() - 1); // Убираем "!"
                        String[] com = buf.split(" ");
                        if (com.length != 2) {
                            throw new ParserException();
                        }
                        if (com[0].equals("if")) {
                            state = 2; // Следующий - тело if
                            buf = com[1];
                        } else if (com[0].equals("repeat")) {
                            state = 4; // Следующий - тело for
                            buf = com[1];
                        } else {
                            throw new ParserException();
                        }
                    } else { // Вставка по имени
                        state = 0; // Следующий - обычный текст
                        build.add(new StrSimpleBuilder(arr[i]));
                    }
                    break;
                case 2:
                    build.add(new StrIfBuilder(buf, arr[i]));
                    state = 3; // Следующий - окончание if
                    break;
                case 3:
                    if (!arr[i].equals("!endif!")) {
                        throw new ParserException();
                    }
                    state = 0; // Следующий - обычный текст
                    break;
                case 4:
                    build.add(new StrForBuilder(buf, arr[i]));
                    state = 5;
                    break;
                case 5:
                    if (!arr[i].equals("!endrepeat!")) {
                        throw new ParserException();
                    }
                    state = 0;
                    break;
            }
        }
        this.arr = new StrBaseBuilder[build.size()];
        build.toArray(this.arr);
    }

    String fill(
            Map<String, String> values,
            Map<String, Boolean> conditions,
            Map<String, Integer> iterations)
            throws ArgumentNotFoundException {
        DataPacket data = new DataPacket(false);
        data.iters = iterations;
        data.conds = conditions;
        data.values = values;
        return fill(data);
    }

    String fill(DataPacket data) throws ArgumentNotFoundException {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < arr.length; i++) {
            arr[i].get(data, str);
        }
        return str.toString();
    }
}
