package ru.nsu.fit.cherniak.lab1.stuff.builders;

import javax.xml.crypto.Data;
import java.util.HashMap;
import java.util.Map;

/*
Для более простой передачи параметров
 */
public class DataPacket {
    public Map<String, String> values;
    public Map<String, Boolean> conds;
    public Map<String, Integer> iters;

    public DataPacket() {
        values = new HashMap<>();
        conds = new HashMap<>();
        iters = new HashMap<>();
    }

    public DataPacket(boolean b) {
    } // Не создает новые объекты
}
