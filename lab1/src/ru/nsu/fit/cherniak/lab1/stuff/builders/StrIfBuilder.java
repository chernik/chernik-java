package ru.nsu.fit.cherniak.lab1.stuff.builders;

import ru.nsu.fit.cherniak.lab1.stuff.builders.DataPacket;
import ru.nsu.fit.cherniak.lab1.stuff.builders.StrBaseBuilder;
import ru.nsu.fit.cherniak.lab1.stuff.exc.ArgumentNotFoundException;

public class StrIfBuilder implements StrBaseBuilder {

    private String name;
    private String data;

    public StrIfBuilder(String name, String data) {
        this.name = name;
        this.data = data;
    }

    @Override
    public void get(DataPacket data, StringBuilder str) throws ArgumentNotFoundException {
        if (!data.conds.containsKey(name)) {
            throw new ArgumentNotFoundException();
        }
        if (data.conds.get(name)) {
            str.append(this.data);
        }
    }
}
