package ru.nsu.fit.cherniak.lab1.stuff.builders;

public class StrDataBuilder implements StrBaseBuilder {

    String data;

    public StrDataBuilder(String data) {
        this.data = data;
    }

    @Override
    public void get(DataPacket data, StringBuilder str) {
        str.append(this.data);
    }
}