package ru.nsu.fit.cherniak.lab1.stuff.builders;

import ru.nsu.fit.cherniak.lab1.stuff.exc.ArgumentNotFoundException;

public interface StrBaseBuilder {
    public void get(DataPacket data, StringBuilder str) throws ArgumentNotFoundException;
}
