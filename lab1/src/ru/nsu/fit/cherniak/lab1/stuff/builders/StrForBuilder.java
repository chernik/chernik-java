package ru.nsu.fit.cherniak.lab1.stuff.builders;

import ru.nsu.fit.cherniak.lab1.stuff.builders.DataPacket;
import ru.nsu.fit.cherniak.lab1.stuff.builders.StrBaseBuilder;
import ru.nsu.fit.cherniak.lab1.stuff.exc.ArgumentNotFoundException;

public class StrForBuilder implements StrBaseBuilder {

    private String name;
    private String data;

    public StrForBuilder(String name, String data) {
        this.name = name;
        this.data = data;
    }

    @Override
    public void get(DataPacket data, StringBuilder str) throws ArgumentNotFoundException {
        if (!data.iters.containsKey(name)) {
            throw new ArgumentNotFoundException();
        }
        int count = data.iters.get(name);
        for (int i = 0; i < count; i++) {
            str.append(this.data);
        }
    }
}
