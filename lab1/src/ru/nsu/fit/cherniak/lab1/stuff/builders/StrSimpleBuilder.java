package ru.nsu.fit.cherniak.lab1.stuff.builders;

import ru.nsu.fit.cherniak.lab1.stuff.builders.DataPacket;
import ru.nsu.fit.cherniak.lab1.stuff.builders.StrBaseBuilder;
import ru.nsu.fit.cherniak.lab1.stuff.exc.ArgumentNotFoundException;

public class StrSimpleBuilder implements StrBaseBuilder {

    String name;

    public StrSimpleBuilder(String name) {
        this.name = name;
    }

    @Override
    public void get(DataPacket data, StringBuilder str) throws ArgumentNotFoundException {
        if (!data.values.containsKey(name)) {
            throw new ArgumentNotFoundException();
        }
        str.append(
                data.values.get(name)
        );
    }
}
