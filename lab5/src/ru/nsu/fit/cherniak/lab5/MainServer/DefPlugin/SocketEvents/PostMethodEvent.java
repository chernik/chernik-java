package ru.nsu.fit.cherniak.lab5.MainServer.DefPlugin.SocketEvents;

import ru.nsu.fit.cherniak.lab5.MainServer.FileServer;
import ru.nsu.fit.cherniak.lab5.Server.exc.ExceptionHttpParser;
import ru.nsu.fit.cherniak.lab5.Server.exc.ExceptionMultipartParser;
import ru.nsu.fit.cherniak.lab5.Server.parse.HttpParser;
import ru.nsu.fit.cherniak.lab5.Server.parse.LinkParser;
import ru.nsu.fit.cherniak.lab5.Server.parse.MultipartParser;
import ru.nsu.fit.cherniak.lab5.Server.stuff.HttpSender;
import ru.nsu.fit.cherniak.lab5.Server.stuff.MultipartContainer;
import ru.nsu.fit.cherniak.lab5.Server.stuff.SocketEvent;
import ru.nsu.fit.cherniak.lab5.Stuff;

import java.util.Map;

/**
 * Обработчик пришедшего сокета
 */
public class PostMethodEvent implements SocketEvent {
    private HttpParser httpParser;
    private HttpSender httpSender;
    private FileServer fileServer;

    public PostMethodEvent(FileServer fileServer) {
        this.fileServer = fileServer;
    }

    /**
     * Обработка пришедшего пакета
     *
     * @param httpParser данные клиента
     * @param httpSender данные для отправки
     */
    @Override
    public void onSocket(HttpParser httpParser, HttpSender httpSender) {
        this.httpParser = httpParser;
        this.httpSender = httpSender;
        postEvent();
    }

    /**
     * Обработка POST запросов
     */
    private void postEvent() {
        String fullPostType;
        try {
            fullPostType = httpParser.getValue("Content-Type");
        } catch (ExceptionHttpParser exceptionHttpParser) {
            httpSender.sendError("cannot get <i>Content-Type</i><br>" + exceptionHttpParser.get(), 405);
            return;
        }
        String postType = fullPostType.split("; ")[0];
        switch (postType) {
            case "":
                httpSender.sendError("empty <i>Content-Type</i>", 405);
                break;
            case "multipart/form-data":
                postMultipartEvent(fullPostType);
                break;
            case "application/x-www-form-urlencoded":
                postLikeGetEvent();
                break;
            default:
                httpSender.sendError("unknown type <i>" + postType + "</i>", 405);
                break;
        }
    }

    /**
     * Обработка простых POST запросов
     */
    private void postLikeGetEvent() {
        LinkParser postParser;
        try {
            postParser = new LinkParser("?" + (new String(httpParser.getBody())));
        } catch (ExceptionHttpParser exceptionHttpParser) {
            httpSender.sendError("cannot parse body<br>" + exceptionHttpParser.get(), 405);
            return;
        }
        httpSender.sendData(
                Stuff.getViewOfMap(postParser.getMap(), "<br>")
        );
    }

    /**
     * Обработка Multipart POST запросов
     *
     * @param fullPostType значение Content-Type из заголовка
     */
    private void postMultipartEvent(String fullPostType) {
        byte[] body;
        try {
            body = httpParser.getBody();
        } catch (ExceptionHttpParser exceptionHttpParser) {
            httpSender.sendError("cannot get body<br>" + exceptionHttpParser.get(), 405);
            return;
        }
        String res = getViewMultipart(
                body,
                fullPostType.split("boundary=")[1].split("; ")[0]
        );
        httpSender.sendData(res);
    }

    /**
     * Проверка на необходимость загружать файл на сервер
     * Не знаю, зачем я это вынес, видимо из-за вложенных if с большими условиями
     *
     * @param parser данные из Multipart
     * @return null в случае успеха и описание ошибки в ином случае
     */
    private String isUploadEvent(MultipartParser parser) {
        MultipartContainer mc;
        mc = parser.getPart("text");
        if (
                mc != null
                        && mc.getMimeType().length() == 0
                        && (new String(mc.getData())).equals("upload")) {
            mc = parser.getPart("file");
            if (
                    mc != null
                            && mc.getVar("filename") != null
                    ) {
                return uploadEvent(
                        Stuff.delQuotes(mc.getVar("filename")),
                        //mc.getMimeType(),
                        mc.getData()
                );
            }
        }
        return null;
    }

    /**
     * Загрузка файла
     *
     * @param name имя файла
     * @param data данные из файла
     * @return null в случае успеха, описание ошибки в случае ошибки
     */
    private String uploadEvent(String name, byte[] data) {
        String resName;
        if ((resName = fileServer.saveFile(data, name)) == null) {
            return "Error in load file";
        }
        return "Loaded in file '<u>" + resName + "</u>'";
    }

    /**
     * Формирование ответа на принятый Multipart
     *
     * @param data  данные из POST
     * @param delim разделитель Multipart
     * @return ответ
     */
    private String getViewMultipart(byte[] data, String delim) {
        MultipartParser parser;
        try {
            parser = new MultipartParser(data, delim);
        } catch (ExceptionMultipartParser exceptionMultipartParser) {
            return "cannot parse multipart<br>" + exceptionMultipartParser.get();
        }
        String res = Stuff.getViewOfMultipart(parser.getMap());
        String err = isUploadEvent(parser);
        if (err != null) {
            return "UPLOAD LOG<br>" + err;
        }
        return res;
    }

    /**
     * Вывод таблицы в консоль
     * Используется для отладки
     *
     * @param map  балица
     * @param head название
     */
    private void traceMap(Map<String, String> map, String head) {
        System.out.println(head + " >");
        System.out.print(Stuff.getViewOfMap(map, "\n"));
        System.out.println("---------------\n");
    }
}
