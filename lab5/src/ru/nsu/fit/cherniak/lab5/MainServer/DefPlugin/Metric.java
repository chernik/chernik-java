package ru.nsu.fit.cherniak.lab5.MainServer.DefPlugin;

import ru.nsu.fit.cherniak.lab5.Server.parse.HttpParser;
import ru.nsu.fit.cherniak.lab5.Server.stuff.HttpSender;

import java.util.Date;

/**
 * Контейнер для метрики
 */
public class Metric {
    public Date start;
    public Date finish;
    public HttpParser httpParser;
    public HttpSender httpSender;

    @Override
    public String toString(){
        return "Start at <i>" + start.toString() + "</i><br>" +
                "Time " + (finish.getTime() - start.getTime()) + " ms<br>" +
                "<h1>Request ></h1><br>" + httpParser.toString() + "<br>" +
                "<h1>Response ></h1><br>" + httpSender.toString() + "<br>" +
                "<hr>";
    }
}
