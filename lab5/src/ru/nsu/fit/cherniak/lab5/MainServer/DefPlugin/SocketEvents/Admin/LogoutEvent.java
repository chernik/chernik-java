package ru.nsu.fit.cherniak.lab5.MainServer.DefPlugin.SocketEvents.Admin;

import ru.nsu.fit.cherniak.lab5.MainServer.DefPlugin.AdminContext;
import ru.nsu.fit.cherniak.lab5.Server.parse.HttpParser;
import ru.nsu.fit.cherniak.lab5.Server.stuff.HttpSender;
import ru.nsu.fit.cherniak.lab5.Server.stuff.SocketEvent;

public class LogoutEvent implements SocketEvent {
    private AdminContext adminContext;

    public LogoutEvent(AdminContext adminContext) {
        this.adminContext = adminContext;
    }

    @Override
    public void onSocket(HttpParser httpParser, HttpSender httpSender) {
        adminContext.logout();
        httpSender.sendData("Logouted");
    }
}
