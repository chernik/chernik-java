package ru.nsu.fit.cherniak.lab5.MainServer.GUI;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Интерфейс обработки нажатия кнопки
 */
public interface EventClick extends MouseListener, Runnable {

    @Override
    default void mouseClicked(MouseEvent e) {
        run();
    }

    @Override
    default void mousePressed(MouseEvent e) {

    }

    @Override
    default void mouseReleased(MouseEvent e) {

    }

    @Override
    default void mouseEntered(MouseEvent e) {

    }

    @Override
    default void mouseExited(MouseEvent e) {

    }
}
