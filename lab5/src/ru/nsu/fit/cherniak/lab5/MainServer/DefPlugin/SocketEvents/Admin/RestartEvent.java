package ru.nsu.fit.cherniak.lab5.MainServer.DefPlugin.SocketEvents.Admin;

import ru.nsu.fit.cherniak.lab5.MatherServer.EnvironSocketEvent;
import ru.nsu.fit.cherniak.lab5.MatherServer.ServerContext;
import ru.nsu.fit.cherniak.lab5.Server.parse.HttpParser;
import ru.nsu.fit.cherniak.lab5.Server.stuff.HttpSender;

public class RestartEvent implements EnvironSocketEvent {
    private ServerContext serverContext;

    @Override
    public void onSocket(HttpParser httpParser, HttpSender httpSender) {
        httpSender.sendData("Restarting started");
        serverContext.getServer().restart();
    }

    @Override
    public void saveEnviron(ServerContext serverContext) {
        this.serverContext = serverContext;
    }
}
