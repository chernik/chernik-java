package ru.nsu.fit.cherniak.lab5.MainServer;

import java.util.Map;

public class DataBase {
    private static Map<String, String> map = null;

    synchronized public static boolean put(String key, String value){
        if(map == null){
            map = FileServer.loadDataBase();
        }
        map.put(key, value);
        return FileServer.saveDataBase(map);
    }

    synchronized public static String getOrDefault(String key, String def){
        if(map == null){
            map = FileServer.loadDataBase();
        }
        return map.getOrDefault(key, def);
    }
}
