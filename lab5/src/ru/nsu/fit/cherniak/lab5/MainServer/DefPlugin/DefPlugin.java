package ru.nsu.fit.cherniak.lab5.MainServer.DefPlugin;

import ru.nsu.fit.cherniak.lab5.MainServer.DefPlugin.Interceptors.*;
import ru.nsu.fit.cherniak.lab5.MainServer.DefPlugin.SocketEvents.Admin.*;
import ru.nsu.fit.cherniak.lab5.MainServer.DefPlugin.SocketEvents.FileServerEvent;
import ru.nsu.fit.cherniak.lab5.MainServer.DefPlugin.SocketEvents.PostMethodEvent;
import ru.nsu.fit.cherniak.lab5.MainServer.FileServer;
import ru.nsu.fit.cherniak.lab5.MatherServer.EnderContainer;
import ru.nsu.fit.cherniak.lab5.MatherServer.Interceptors.PipeLine;
import ru.nsu.fit.cherniak.lab5.MatherServer.Interceptors.UnorderedPipeLine;
import ru.nsu.fit.cherniak.lab5.MatherServer.MatherHttpServer;
import ru.nsu.fit.cherniak.lab5.MatherServer.Mathers.AnyMather;
import ru.nsu.fit.cherniak.lab5.MatherServer.Mathers.IntMather;
import ru.nsu.fit.cherniak.lab5.MatherServer.Mathers.PathMather;
import ru.nsu.fit.cherniak.lab5.MatherServer.Mathers.StringMather;
import ru.nsu.fit.cherniak.lab5.MatherServer.Plugins.HttpServerPlugin;
import ru.nsu.fit.cherniak.lab5.MatherServer.PointContainer;
import ru.nsu.fit.cherniak.lab5.MatherServer.ServerContext;
import ru.nsu.fit.cherniak.lab5.Server.parse.HttpParser;
import ru.nsu.fit.cherniak.lab5.Server.stuff.HttpSender;
import ru.nsu.fit.cherniak.lab5.Stuff;

import java.util.Arrays;
import java.util.Collections;

public class DefPlugin implements HttpServerPlugin {

    private void outMap(HttpParser httpParser, HttpSender httpSender) {
        httpSender.sendData(
                Stuff.getViewOfMap(httpParser.getPathInfo(), "<br>")
        );
    }

    @Override
    public void initialize(ServerContext serverContext) {
        MatherHttpServer server = serverContext.getServer();
        PipeLine pipeLine;
        FileServer fileServer = new FileServer();
        // Metrics
        Metrics metrics = new Metrics();
        server.registerTop(new UnorderedPipeLine(Collections.singletonList(
                new MetricInInterceptor(metrics)
        ), Collections.singletonList(
                new MetricOutInterceptor(metrics)
        )));
        // Test tree
        EnderContainer forOutMap = new EnderContainer(() -> this::outMap);
        server.register(Arrays.asList(
                new StringMather("name", null),
                new AnyMather("name")
        ), forOutMap);
        server.register(Arrays.asList(
                new StringMather("name", null),
                new AnyMather("name"),
                new StringMather("height", "parameter"),
                new IntMather("height")
        ), forOutMap);
        server.register(Arrays.asList(
                new StringMather("name", null),
                new AnyMather("name"),
                new StringMather("surname", "parameter"),
                new AnyMather("surname")
        ), forOutMap);
        // POST method
        pipeLine = new PipeLine();
        pipeLine.getInners()
                .addBack(new MethodInterceptor("POST"));
        server.register(Collections.singletonList(
                new StringMather("post", null)
        ), new EnderContainer(
                () -> new PostMethodEvent(fileServer),
                pipeLine
        ));
        // GET method
        pipeLine = new PipeLine();
        pipeLine.getInners()
                .addBack(new MethodInterceptor("GET"))
                .addBack(new EchoInterceptor());
        server.register(Collections.singletonList(
                new PathMather("path", -1)
        ), new EnderContainer(
                () -> new FileServerEvent(fileServer),
                pipeLine
        ));
        // ADMIN
        AdminContext adminContext = new AdminContext();
        server.registerPoints(Collections.singletonList(
                new PointContainer(
                        new StringMather("admin", null),
                        new UnorderedPipeLine(new LoginInterceptor(adminContext))
                )
        ), new EnderContainer(
                ErrorEvent::new
        ));
        server.register(Arrays.asList(
                new StringMather("admin", null),
                new StringMather("logout", null)
        ), new EnderContainer(
                () -> new LogoutEvent(adminContext)
        ));
        server.register(Arrays.asList(
                new StringMather("admin", null),
                new StringMather("ping", null)
        ), new EnderContainer(PingEvent::new));
        server.register(Arrays.asList(
                new StringMather("admin", null),
                new StringMather("changePass", null)
        ), new EnderContainer(
                () -> new ChangePassEvent(adminContext)
        ));
        server.register(Arrays.asList(
                new StringMather("admin", null),
                new StringMather("metrics", null)
        ), new EnderContainer(
                () -> new MetricsEvent(metrics)
        ));
        server.register(Arrays.asList(
                new StringMather("admin", null),
                new StringMather("restart", null)
        ), new EnderContainer(RestartEvent::new));
        server.register(Arrays.asList(
                new StringMather("admin", null),
                new StringMather("registerPlugin", null)
        ), new EnderContainer(
                () -> new NewPluginEvent(fileServer)
        ));
        server.register(Arrays.asList(
                new StringMather("admin", null),
                new StringMather("getListPlugins", null)
        ), new EnderContainer(ListPluginsEvent::new));
    }

    @Override
    public String getName() {
        return "Default plugin";
    }
}
