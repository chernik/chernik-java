package ru.nsu.fit.cherniak.lab5.MainServer.DefPlugin.SocketEvents;

import ru.nsu.fit.cherniak.lab5.MainServer.FileServer;
import ru.nsu.fit.cherniak.lab5.Server.parse.HttpParser;
import ru.nsu.fit.cherniak.lab5.Server.stuff.HttpSender;
import ru.nsu.fit.cherniak.lab5.Server.stuff.SocketEvent;

import java.io.File;

public class FileServerEvent implements SocketEvent {
    FileServer fileServer;

    public FileServerEvent(FileServer fileServer) {
        this.fileServer = fileServer;
    }

    @Override
    public void onSocket(HttpParser httpParser, HttpSender httpSender) {
        String path = httpParser.getPathInfo().get("path");
        File f = fileServer.getFile(path);
        if (f == null) {
            httpSender.sendError("not found <i>" + httpParser.getLinkParser().getPath() + "</i>", 404);
            return;
        }
        byte[] dataFile = fileServer.loadBinary(f);
        if (dataFile == null) {
            httpSender.sendError("cannot read file <i>" + httpParser.getLinkParser().getPath() + "</i>", 404);
            return;
        }
        String mime = fileServer.getMimeType(f);
        if (httpParser.getLinkParser().getValue("astext") != null) {
            mime = "text/plain";
        }
        httpSender.sendData(dataFile, mime);
    }
}
