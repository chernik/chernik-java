package ru.nsu.fit.cherniak.lab5.MainServer.DefPlugin.Interceptors;

import ru.nsu.fit.cherniak.lab5.MainServer.DefPlugin.Metric;
import ru.nsu.fit.cherniak.lab5.MainServer.DefPlugin.Metrics;
import ru.nsu.fit.cherniak.lab5.MatherServer.Interceptors.AutoinsertableInterceptor;
import ru.nsu.fit.cherniak.lab5.MatherServer.Interceptors.Interceptor;
import ru.nsu.fit.cherniak.lab5.MatherServer.Interceptors.ListInterceptors;
import ru.nsu.fit.cherniak.lab5.Server.parse.HttpParser;
import ru.nsu.fit.cherniak.lab5.Server.stuff.HttpSender;

import java.util.Date;

public class MetricOutInterceptor implements AutoinsertableInterceptor {
    private Metrics metrics;

    public MetricOutInterceptor(Metrics metrics) {
        this.metrics = metrics;
    }

    @Override
    public boolean check(HttpParser httpParser, HttpSender httpSender) {
        Metric metric = metrics.stopMetric(httpParser);
        metric.finish = new Date();
        metric.httpSender = httpSender;
        metric.httpParser = httpParser;
        return false;
    }

    @Override
    public String getName() {
        return "Outer metric";
    }

    @Override
    public void insert(ListInterceptors listInterceptors) {
        listInterceptors.addBack(this);
    }
}
