package ru.nsu.fit.cherniak.lab5.MainServer.DefPlugin.Interceptors;

import ru.nsu.fit.cherniak.lab5.MatherServer.Interceptors.Interceptor;
import ru.nsu.fit.cherniak.lab5.Server.exc.ExceptionHttpParser;
import ru.nsu.fit.cherniak.lab5.Server.parse.HttpParser;
import ru.nsu.fit.cherniak.lab5.Server.stuff.HttpSender;

public class MethodInterceptor implements Interceptor {
    private String method;

    public MethodInterceptor(String method) {
        this.method = method;
    }

    @Override
    public boolean check(HttpParser httpParser, HttpSender httpSender) {
        String method;
        try {
            method = httpParser.getType();
        } catch (ExceptionHttpParser exceptionHttpParser) {
            httpSender.sendError("parser error > " + exceptionHttpParser.get(), 405);
            return true;
        }
        if (method.equals(this.method)) {
            return false;
        }
        httpSender.sendError("unknown method " + method, 405);
        return true;
    }

    @Override
    public String getName() {
        return "Get method interceptor";
    }
}
