package ru.nsu.fit.cherniak.lab5.MainServer.DefPlugin.SocketEvents.Admin;

import ru.nsu.fit.cherniak.lab5.MainServer.DefPlugin.AdminContext;
import ru.nsu.fit.cherniak.lab5.Server.exc.ExceptionHttpParser;
import ru.nsu.fit.cherniak.lab5.Server.parse.HttpParser;
import ru.nsu.fit.cherniak.lab5.Server.stuff.HttpSender;
import ru.nsu.fit.cherniak.lab5.Server.stuff.SocketEvent;

public class ChangePassEvent implements SocketEvent {
    private AdminContext adminContext;

    public ChangePassEvent(AdminContext adminContext) {
        this.adminContext = adminContext;
    }

    @Override
    public void onSocket(HttpParser httpParser, HttpSender httpSender) {
        String pass;
        try {
            pass = httpParser.getValue("par");
        } catch (ExceptionHttpParser exceptionHttpParser) {
            httpSender.sendError("Error in get pass", 402);
            return;
        }
        if (pass.equals("")) {
            httpSender.sendError("Cannot get pass", 402);
            return;
        }
        adminContext.setPass(pass);
        httpSender.sendData("Password successfully changed");
    }
}
