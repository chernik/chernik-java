package ru.nsu.fit.cherniak.lab5.MainServer.DefPlugin.SocketEvents.Admin;

import ru.nsu.fit.cherniak.lab5.MatherServer.EnvironSocketEvent;
import ru.nsu.fit.cherniak.lab5.MatherServer.Plugins.ErrorPlugin;
import ru.nsu.fit.cherniak.lab5.MatherServer.Plugins.HttpServerPlugin;
import ru.nsu.fit.cherniak.lab5.MatherServer.ServerContext;
import ru.nsu.fit.cherniak.lab5.Server.parse.HttpParser;
import ru.nsu.fit.cherniak.lab5.Server.stuff.HttpSender;

import java.util.stream.Collectors;

public class ListPluginsEvent implements EnvironSocketEvent {
    private ServerContext serverContext;

    @Override
    public void onSocket(HttpParser httpParser, HttpSender httpSender) {
        String res = String.join("<br>", serverContext.getServer()
                .getPlugins()
                .stream()
                .map(plg -> {
                    String str = plg.getName() + "<br>" + plg.getClass().getName();
                    if(plg instanceof ErrorPlugin){
                        str += "<br>Path > " + ((ErrorPlugin) plg).getPath();
                    }
                    return str;
                })
                .map(s -> "<li>" + s + "</li>")
                .collect(Collectors.toList()));
        httpSender.sendData("<html><body>" +
                "List of plugins<br><br><ul>" +
                res + "</ul></body></html>"
        );
    }

    @Override
    public void saveEnviron(ServerContext serverContext) {
        this.serverContext = serverContext;
    }
}
