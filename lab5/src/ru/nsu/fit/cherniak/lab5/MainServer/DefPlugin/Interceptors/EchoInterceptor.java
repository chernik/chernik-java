package ru.nsu.fit.cherniak.lab5.MainServer.DefPlugin.Interceptors;

import ru.nsu.fit.cherniak.lab5.MatherServer.Interceptors.Interceptor;
import ru.nsu.fit.cherniak.lab5.Server.parse.HttpParser;
import ru.nsu.fit.cherniak.lab5.Server.stuff.HttpSender;

public class EchoInterceptor implements Interceptor {
    @Override
    public boolean check(HttpParser httpParser, HttpSender httpSender) {
        String echo = httpParser.getLinkParser().getValue("echo");
        if (echo == null) {
            return false;
        }
        httpSender.sendData(echo);
        return true;
    }

    @Override
    public String getName() {
        return "get/echo";
    }
}
