package ru.nsu.fit.cherniak.lab5.MainServer.DefPlugin.SocketEvents.Admin;

import ru.nsu.fit.cherniak.lab5.MainServer.FileServer;
import ru.nsu.fit.cherniak.lab5.MatherServer.EnvironSocketEvent;
import ru.nsu.fit.cherniak.lab5.MatherServer.Plugins.HttpServerPlugin;
import ru.nsu.fit.cherniak.lab5.MatherServer.Plugins.PluginLoader;
import ru.nsu.fit.cherniak.lab5.MatherServer.Plugins.PluginLoaderError;
import ru.nsu.fit.cherniak.lab5.MatherServer.ServerContext;
import ru.nsu.fit.cherniak.lab5.Server.exc.ExceptionHttpParser;
import ru.nsu.fit.cherniak.lab5.Server.parse.HttpParser;
import ru.nsu.fit.cherniak.lab5.Server.stuff.HttpSender;
import ru.nsu.fit.cherniak.lab5.Server.stuff.SocketEvent;

import java.io.File;

public class NewPluginEvent implements EnvironSocketEvent {
    private FileServer fileServer;
    private ServerContext serverContext;

    public NewPluginEvent(FileServer fileServer) {
        this.fileServer = fileServer;
    }

    @Override
    public void onSocket(HttpParser httpParser, HttpSender httpSender) {
        String jarName;
        try {
            jarName = httpParser.getValue("par");
        } catch (ExceptionHttpParser exceptionHttpParser) {
            httpSender.sendError("Error in get file name", 402);
            return;
        }
        if(jarName.equals("")){
            httpSender.sendError("Cannot get file name", 402);
            return;
        }
        if(jarName.equals("HELP")){
            httpSender.sendData("<html><body><ul>" +
                    "<li>Go here: <a href=\"/www/post.htm\">/www/post.htm</li>" +
                    "<li>In end of page in text field replace 'text' -> 'upload'</li>" +
                    "<li>Click on button 'Upload' and choose your file</li>" +
                    "<li>Click button 'Send'</li>" +
                    "<li>Remember file name in line <u>Loaded in file 'FILENAME'</u></li></ul></body></html>"
            );
            return;
        }
        File f = new File(fileServer.getUploadedName() + "/" + jarName);
        HttpServerPlugin httpServerPlugin;
        try {
            httpServerPlugin = PluginLoader.loadPlugin(f);
        } catch (PluginLoaderError pluginLoaderError) {
            httpSender.sendError("Plugin loader error<br>" + pluginLoaderError.get(), 402);
            return;
        }
        serverContext.getServer().registerPlugin(httpServerPlugin);
        httpSender.sendData("Plugin registered");
    }

    @Override
    public void saveEnviron(ServerContext serverContext) {
        this.serverContext = serverContext;
    }
}
