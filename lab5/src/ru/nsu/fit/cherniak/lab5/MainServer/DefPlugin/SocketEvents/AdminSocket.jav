package ru.nsu.fit.cherniak.lab5.MainServer.DefPlugin.SocketEvents;

import ru.nsu.fit.cherniak.lab5.MainServer.DefPlugin.AdminContext;
import ru.nsu.fit.cherniak.lab5.MainServer.DefPlugin.Metric;
import ru.nsu.fit.cherniak.lab5.MainServer.DefPlugin.Metrics;
import ru.nsu.fit.cherniak.lab5.MatherServer.EnvironSocketEvent;
import ru.nsu.fit.cherniak.lab5.MatherServer.Plugins.HttpServerPlugin;
import ru.nsu.fit.cherniak.lab5.MatherServer.Plugins.PluginLoader;
import ru.nsu.fit.cherniak.lab5.MatherServer.Plugins.PluginLoaderError;
import ru.nsu.fit.cherniak.lab5.MatherServer.ServerContext;
import ru.nsu.fit.cherniak.lab5.Server.exc.ExceptionHttpParser;
import ru.nsu.fit.cherniak.lab5.Server.parse.HttpParser;
import ru.nsu.fit.cherniak.lab5.Server.stuff.HttpSender;
import ru.nsu.fit.cherniak.lab5.MainServer.FileServer;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class AdminSocket implements EnvironSocketEvent {
    private ServerContext serverContext;
    private HttpParser httpParser;
    private HttpSender httpSender;
    private AdminContext adminContext;
    private String action;
    private Metrics metrics;
    private FileServer fileServer;

    public AdminSocket(AdminContext adminContext, Metrics metrics, FileServer fileServer) {
        serverContext = null;
        this.adminContext = adminContext;
        this.metrics = metrics;
        this.fileServer = fileServer;
    }

    private boolean getAction() {
        try {
            action = httpParser.getValue("action");
        } catch (ExceptionHttpParser exceptionHttpParser) {
            httpSender.sendError("Cannot get action", 402);
            return false;
        }
        if (action.equals("")) {
            httpSender.sendError("Cannot get action<br>Do not open this in browser", 402);
            return false;
        }
        return true;
    }

    private void goLogin() {
        /*
        String user;
        try {
            user = httpParser.getValue("user");
        } catch (ExceptionHttpParser exceptionHttpParser) {
            httpSender.sendError("Cannog get field 'user'", 402);
            return;
        }
        if (!adminContext.authorise(user)) {
            httpSender.sendError("Cannot login", 402);
            return;
        }
        httpSender.setVar("hash", adminContext.getLogHash());
        httpSender.sendData("You logged");
        */
    }

    private boolean goCheck() {
        /*
        String hash;
        try {
            hash = httpParser.getValue("hash");
        } catch (ExceptionHttpParser exceptionHttpParser) {
            httpSender.sendError("Cannot get hash", 402);
            return false;
        }
        if (!adminContext.check(hash)) {
            httpSender.sendError("Wrong hash", 402);
            return false;
        }
        */
        return true;
    }

    @Override
    public void onSocket(HttpParser httpParser, HttpSender httpSender) {
        this.httpParser = httpParser;
        this.httpSender = httpSender;
        if (!getAction()) {
            return;
        }
        if (action.equals("Login")) {
            goLogin();
            return;
        }
        if (!goCheck()) {
            return;
        }
        switch (action) {
            case "Logout":
                goLogout();
                break;
            case "Ping":
                goPing();
                break;
            case "Change password":
                goChangePass();
                break;
            case "Metrics":
                goMetrics();
                break;
            case "Restart":
                goRestart();
                break;
            case "Register plugin":
                goNewPlugin();
                break;
            default:
                httpSender.sendError("Wrong action", 402);
        }
    }

    private void goLogout() {
        adminContext.logout();
        httpSender.sendData("Logouted");
    }

    private void goPing() {
        httpSender.sendData("Ping");
    }

    private void goChangePass() {
        String pass;
        try {
            pass = httpParser.getValue("par");
        } catch (ExceptionHttpParser exceptionHttpParser) {
            httpSender.sendError("Error in get pass", 402);
            return;
        }
        if (pass.equals("")) {
            httpSender.sendError("Cannot get pass", 402);
            return;
        }
        adminContext.setPass(pass);
        httpSender.sendData("Password successfully changed");
    }

    private void goMetrics() {
        httpSender.sendData("<html><body>" + metrics.toString() + "</body></html>");
    }

    private void goRestart() {
        httpSender.sendData("Restarting started");
        serverContext.getServer().restart();
    }

    private void goNewPlugin(){
        String jarName;
        try {
            jarName = httpParser.getValue("par");
        } catch (ExceptionHttpParser exceptionHttpParser) {
            httpSender.sendError("Error in get file name", 402);
            return;
        }
        if(jarName.equals("")){
            httpSender.sendError("Cannot get file name", 402);
            return;
        }
        if(jarName.equals("HELP")){
            httpSender.sendData("<html><body><ul>" +
                    "<li>Go here: <a href=\"/www/post.htm\">/www/post.htm</li>" +
                    "<li>In end of page in text field replace 'text' -> 'upload'</li>" +
                    "<li>Click on button 'Upload' and choose your file</li>" +
                    "<li>Click button 'Send'</li>" +
                    "<li>Remember file name in line <u>Loaded in file 'FILENAME'</u></li></ul></body></html>"
            );
            return;
        }
        File f = new File(fileServer.getUploadedName() + "/" + jarName);
        HttpServerPlugin httpServerPlugin;
        try {
            httpServerPlugin = PluginLoader.loadPlugin(f);
        } catch (PluginLoaderError pluginLoaderError) {
            httpSender.sendError("Plugin loader error<br>" + pluginLoaderError.get(), 402);
            return;
        }
        serverContext.getServer().registerPlugin(httpServerPlugin);
        httpSender.sendData("Plugin registered");
    }

    @Override
    public void saveEnviron(ServerContext serverContext) {
        this.serverContext = serverContext;
    }
}
