package ru.nsu.fit.cherniak.lab5.MainServer.GUI;

import ru.nsu.fit.cherniak.lab5.MainServer.DefPlugin.DefPlugin;
import ru.nsu.fit.cherniak.lab5.MatherServer.MatherHttpServer;
import ru.nsu.fit.cherniak.lab5.Server.exc.ExceptionHttpServer;

import javax.swing.*;
import java.awt.*;

/**
 * Класс для гуишного окна
 */
public class GUIFrame extends JFrame {
    private JLabel log;
    private JButton butStart, butStop;

    private MatherHttpServer server;

    /**
     * Конструктор окна и сервера
     */
    public GUIFrame() {
        server = new MatherHttpServer(4);
        server.registerPlugin(new DefPlugin());
        start();
    }

    /**
     * Установка элементов окна
     */
    private void start() {
        // Окно
        setDefaultLookAndFeelDecorated(true);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        setPreferredSize(new Dimension(300, 500));
        setAlwaysOnTop(true);
        setVisible(true);
        pack();
        addWindowListener(new CloseFrameEvent(this, server));
        setLocationRelativeTo(null);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        screenSize.width -= getWidth();
        screenSize.height -= getHeight() + 50;
        setLocation(screenSize.width, screenSize.height);
        // Layout
        Container lout = this.getContentPane();
        lout.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.ipadx = 200;
        // Панель логгинга
        log = new JLabel();
        log.setText("<empty>");
        c.gridy = 0;
        c.ipady = 20;
        lout.add(log, c);
        // Таймер для логгинга
        Timer tm = new Timer(100, e -> tmEvent());
        // Кнопка старта сервера
        butStart = new JButton();
        butStart.setText("Start");
        butStart.addMouseListener((EventClick) this::butStartEvent);
        c.gridy = 1;
        c.ipady = 20;
        lout.add(butStart, c);
        // Кнопка остановки сервера
        butStop = new JButton();
        butStop.setText("Stop");
        butStop.addMouseListener((EventClick) this::butStopEvent);
        c.gridy = 2;
        lout.add(butStop, c);
        // Запуск элементов
        tm.start();
        //butStartEvent();
    }

    /**
     * Обработчик таймера
     * Выполняет лог сервера
     */
    private void tmEvent() {
        String state = server.getState();
        if (state.equals("")) {
            state = "<empty>";
        }
        log.setText(state);
        if (server.isEnded()) {
            butStop.setEnabled(false);
            butStart.setEnabled(true);
        } else {
            butStop.setEnabled(true);
            butStart.setEnabled(false);
        }
    }

    /**
     * Обработчик кнопки запуска сервера
     */
    private void butStartEvent() {
        if (!butStart.isEnabled()) {
            return;
        }
        try {
            server.start();
        } catch (ExceptionHttpServer exceptionHttpServer) {
            log.setText("ERROR > at start");
        }
    }

    /**
     * Обработчик кнопки остановки сервера
     */
    private void butStopEvent() {
        if (!butStop.isEnabled()) {
            return;
        }
        try {
            server.stop();
        } catch (ExceptionHttpServer exceptionHttpServer) {
            log.setText("ERROR > stop");
        }
    }
}
