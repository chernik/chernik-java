package ru.nsu.fit.cherniak.lab5.MainServer.DefPlugin.SocketEvents.Admin;

import ru.nsu.fit.cherniak.lab5.MainServer.DefPlugin.Metrics;
import ru.nsu.fit.cherniak.lab5.Server.parse.HttpParser;
import ru.nsu.fit.cherniak.lab5.Server.stuff.HttpSender;
import ru.nsu.fit.cherniak.lab5.Server.stuff.SocketEvent;

public class MetricsEvent implements SocketEvent {
    private Metrics metrics;

    public MetricsEvent(Metrics metrics) {
        this.metrics = metrics;
    }

    @Override
    public void onSocket(HttpParser httpParser, HttpSender httpSender) {
        httpSender.sendData("<html><body>" + metrics.toString() + "</body></html>");
    }
}
