package ru.nsu.fit.cherniak.lab5.MainServer.DefPlugin;

import ru.nsu.fit.cherniak.lab5.Server.parse.HttpParser;
import ru.nsu.fit.cherniak.lab5.Server.stuff.HttpSender;

import java.util.*;

/**
 * Асинхронная метрика
 */
public class Metrics {
    private List<Metric> metrics;
    private Map<HttpParser, Metric> nowMetrics;

    public Metrics(){
        metrics = new ArrayList<>();
        nowMetrics = new HashMap<>();
    }

    synchronized public Metric startMetric(HttpParser httpParser){
        Metric metric = new Metric();
        nowMetrics.put(httpParser, metric);
        return metric;
    }

    synchronized public Metric stopMetric(HttpParser httpParser){
        Metric metric = nowMetrics.remove(httpParser);
        if(metric == null){
            throw new NullPointerException();
        }
        metrics.add(metric);
        return metric;
    }

    synchronized public Metric[] getMetrics(){
        return metrics.toArray(new Metric[metrics.size()]);
    }

    @Override
    public String toString(){
        Metric[] metrics = this.getMetrics();
        StringBuilder sb = new StringBuilder();
        for(Metric metric: metrics){
            sb.append(metric.toString());
        }
        return sb.toString();
    }
}
