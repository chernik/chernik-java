package ru.nsu.fit.cherniak.lab5.MainServer;

import ru.nsu.fit.cherniak.lab5.MainServer.GUI.GUIFrame;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(GUIFrame::new);
    }
}
