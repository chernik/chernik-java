package ru.nsu.fit.cherniak.lab5.MainServer.DefPlugin.Interceptors;

import ru.nsu.fit.cherniak.lab5.MainServer.DefPlugin.Metric;
import ru.nsu.fit.cherniak.lab5.MainServer.DefPlugin.Metrics;
import ru.nsu.fit.cherniak.lab5.MatherServer.Interceptors.AutoinsertableInterceptor;
import ru.nsu.fit.cherniak.lab5.MatherServer.Interceptors.Interceptor;
import ru.nsu.fit.cherniak.lab5.MatherServer.Interceptors.ListInterceptors;
import ru.nsu.fit.cherniak.lab5.Server.exc.ExceptionHttpParser;
import ru.nsu.fit.cherniak.lab5.Server.parse.HttpParser;
import ru.nsu.fit.cherniak.lab5.Server.stuff.HttpSender;

import java.util.Date;
import java.util.Map;

public class MetricInInterceptor implements AutoinsertableInterceptor {
    private Metrics metrics;

    public MetricInInterceptor(Metrics metrics){
        this.metrics = metrics;
    }

    @Override
    public boolean check(HttpParser httpParser, HttpSender httpSender) {
        Metric metric = metrics.startMetric(httpParser);
        metric.start = new Date();
        return false;
    }

    @Override
    public String getName() {
        return "Inner metric";
    }

    @Override
    public void insert(ListInterceptors listInterceptors) {
        listInterceptors.addFront(this);
    }
}
