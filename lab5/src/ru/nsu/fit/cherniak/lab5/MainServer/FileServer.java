package ru.nsu.fit.cherniak.lab5.MainServer;

import ru.nsu.fit.cherniak.lab5.Stuff;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Асинхронный файловый сервер
 * Позволяет находить файлы по пути и загружать их
 */
public class FileServer {
    private String staticName;
    private String defName;
    private String uploadedName;
    private static File dataBase = new File("database.txt");

    public static File getDataBase() {
        return FileServer.dataBase;
    }

    public String getStaticName() {
        return staticName;
    }

    public String getDefName() {
        return defName;
    }

    public String getUploadedName() {
        return uploadedName;
    }

    public FileServer() {
        this.staticName = "static";
        this.defName = "index.htm";
        this.uploadedName = "uploaded";
    }

    public static void setDataBase(File dataBase) {
        FileServer.dataBase = dataBase;
    }

    /**
     * Установка нового пути для папки загрузки
     *
     * @param uploadedName
     */
    public void setUploadedName(String uploadedName) {
        this.uploadedName = uploadedName;
    }

    /**
     * Ищет файл по пути
     *
     * @param path Путь
     * @return Существующий файл либо null в случае ошибки
     */
    synchronized public File getFile(String path) {
        if (path.endsWith("/")) { // По ТЗ
            return null;
        }
        File f;
        String fullPath = this.staticName + (this.staticName.length() == 0 ? "" : "/") + path;
        f = Paths.get(fullPath).toFile();
        if (f.exists() && f.isFile() && f.canRead()) {
            return f;
        }
        f = Paths.get(fullPath + (fullPath.length() == 0 ? "" : "/") + defName).toFile();
        if (!f.exists() || !f.isFile() || !f.canRead()) {
            return null;
        }
        return f;
    }

    /**
     * Взять бинарный файл
     *
     * @param f Файл
     * @return строка на основе массива байт
     */
    synchronized public byte[] loadBinary(File f) {
        FileInputStream fr;
        try {
            fr = new FileInputStream(f);
        } catch (FileNotFoundException e) {
            return null;
        }
        byte[] arr;
        try {
            arr = fr.readAllBytes();
        } catch (IOException e1) {
            arr = null;
        }
        try {
            fr.close();
        } catch (IOException ignored) {

        }
        return arr;
    }

    synchronized public String getMimeType(File f) {
        String mime;
        try {
            mime = Files.probeContentType(Paths.get(f.getPath()));
        } catch (IOException e) {
            mime = "application/binary";
        }
        return mime;
    }

    /**
     * Установка нового пути для папки выгрузки
     *
     * @param path Новый путь
     */
    synchronized public void setStaticName(String path) {
        this.staticName = path;
    }

    /**
     * Установка нового имени по умолчанию
     *
     * @param defName Новое имя
     */
    synchronized public void setDefName(String defName) {
        this.defName = defName;
    }

    /**
     * Загрузить файл на сервер
     *
     * @param data
     * @param name
     * @return
     */
    synchronized public String saveFile(byte[] data, String name) {
        String newName = "[" + (new Date()).toString().replace(':', '-') + "] " + name;
        File f = new File(uploadedName + "/" + newName);
        if (!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException e) {
                return null;
            }
        }
        FileOutputStream os;
        try {
            os = new FileOutputStream(f);
        } catch (FileNotFoundException e) {
            return null;
        }
        try {
            os.write(data);
            os.close();
        } catch (IOException e) {
            return null;
        }
        return f.getName();
    }

    synchronized static public Map<String, String> loadDataBase(){
        Map<String, String> map = new HashMap<>();
        if(!dataBase.exists()){
            return map;
        }
        FileInputStream is;
        try {
            is = new FileInputStream(dataBase);
        } catch (FileNotFoundException e) {
            return map;
        }
        String file;
        try {
            file = new String(is.readAllBytes());
            is.close();
        } catch (IOException e) {
            return map;
        }
        String[] fileSplit = file.split("\n");
        for(String line: fileSplit){
            String[] lineSplit = line.split(": ", 2);
            if(lineSplit.length != 2){
                continue;
            }
            map.put(lineSplit[0], lineSplit[1]);
        }
        return map;
    }

    synchronized static public boolean saveDataBase(Map<String, String> map){
        if(!dataBase.exists()){
            try {
                dataBase.createNewFile();
            } catch (IOException e) {
                return false;
            }
        }
        FileOutputStream os;
        try {
            os = new FileOutputStream(dataBase);
        } catch (FileNotFoundException e) {
            return false;
        }
        StringBuilder sb = new StringBuilder();
        map.forEach((key, value) -> sb.append(key).append(": ").append(value).append("\n"));
        try {
            os.write(sb.toString().getBytes());
            os.close();
        } catch (IOException e) {
            return false;
        }
        return true;
    }
}
