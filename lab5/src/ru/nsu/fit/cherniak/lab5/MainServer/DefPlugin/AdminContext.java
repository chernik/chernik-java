package ru.nsu.fit.cherniak.lab5.MainServer.DefPlugin;

import ru.nsu.fit.cherniak.lab5.MainServer.DataBase;
import ru.nsu.fit.cherniak.lab5.Stuff;

import javax.xml.crypto.Data;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

public class AdminContext {
    private String user;
    private String pass;
    private String hash;
    private int otherTryes;

    public String getUser() {
        return user;
    }

    public String getPass() {
        return pass;
    }

    private String getHash(String str){
        try {
            return Stuff.getMD5(str);
        } catch (NoSuchAlgorithmException e) {
            return str;
        }
    }

    private String getTime(){
        return (new Date()).toString();
    }

    public AdminContext(){
        user = DataBase.getOrDefault("user", "admin");
        pass = DataBase.getOrDefault("pass", getHash("12345"));
        hash = "";
        otherTryes = 0;
    }

    public boolean authorise(String user){
        String[] userParse = user.split(" ", 2);
        if(userParse.length != 2){
            return false;
        }
        if(!userParse[0].equals(pass) || !userParse[1].equals(this.user)){
            return false;
        }
        if(!hash.equals("")){
            otherTryes++;
            return false;
        }
        hash = getHash(getTime() + user);
        return true;
    }

    public String getLogHash(){
        return hash;
    }

    public int getOtherTryes(){
        return otherTryes;
    }

    public void logout(){
        hash = "";
    }

    public boolean check(String hash){
        if(!hash.equals(this.hash)){
            otherTryes++;
            return false;
        }
        return true;
    }

    public void setUser(String user) {
        this.user = user;
        DataBase.put("user", user);
    }

    public void setPass(String pass) {
        this.pass = pass;
        DataBase.put("pass", pass);
    }
}
