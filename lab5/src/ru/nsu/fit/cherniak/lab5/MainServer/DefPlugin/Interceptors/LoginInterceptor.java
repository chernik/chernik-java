package ru.nsu.fit.cherniak.lab5.MainServer.DefPlugin.Interceptors;

import ru.nsu.fit.cherniak.lab5.MainServer.DefPlugin.AdminContext;
import ru.nsu.fit.cherniak.lab5.MatherServer.Interceptors.Interceptor;
import ru.nsu.fit.cherniak.lab5.Server.exc.ExceptionHttpParser;
import ru.nsu.fit.cherniak.lab5.Server.parse.HttpParser;
import ru.nsu.fit.cherniak.lab5.Server.stuff.HttpSender;

/**
 * Login on user/pass with return hash or on hash
 */
public class LoginInterceptor implements Interceptor {
    private AdminContext adminContext;

    public LoginInterceptor(AdminContext adminContext) {
        this.adminContext = adminContext;
    }

    @Override
    public boolean check(HttpParser httpParser, HttpSender httpSender) {
        String value;
        try {
            value = httpParser.getValue("user");
        } catch (ExceptionHttpParser exceptionHttpParser) {
            httpSender.sendError("Cannot get field 'user'", 402);
            return true;
        }
        if(value.equals("")){
            try {
                value = httpParser.getValue("hash");
            } catch (ExceptionHttpParser exceptionHttpParser) {
                httpSender.sendError("Cannot get field 'hash'", 402);
                return true;
            }
            if(value.equals("")){
                httpSender.sendError("No hash<br>Do not open this in browser", 402);
                return true;
            }
            if (!adminContext.check(value)) {
                httpSender.sendError("Wrong hash", 402);
                return true;
            }
            return false;
        }
        if (!adminContext.authorise(value)) {
            httpSender.sendError("Cannot login", 402);
        } else {
            httpSender.setVar("hash", adminContext.getLogHash());
            httpSender.sendData("You logged");
        }
        return true;
    }

    @Override
    public String getName() {
        return "Check login & password";
    }
}
