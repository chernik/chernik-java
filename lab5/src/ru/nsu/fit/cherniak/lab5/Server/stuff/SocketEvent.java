package ru.nsu.fit.cherniak.lab5.Server.stuff;

import ru.nsu.fit.cherniak.lab5.Server.parse.HttpParser;

public interface SocketEvent {
    void onSocket(HttpParser httpParser, HttpSender httpSender);
}
