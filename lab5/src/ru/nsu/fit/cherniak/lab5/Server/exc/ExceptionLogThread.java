package ru.nsu.fit.cherniak.lab5.Server.exc;

/**
 * Ошибка базового класса LogThread
 */
public class ExceptionLogThread extends ExceptionString {
    public ExceptionLogThread(String desc) {
        super(desc);
    }
}
