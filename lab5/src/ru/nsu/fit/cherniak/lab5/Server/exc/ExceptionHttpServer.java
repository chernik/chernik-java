package ru.nsu.fit.cherniak.lab5.Server.exc;

/**
 * Ошибка класса HttpServer
 */
public class ExceptionHttpServer extends ExceptionString {
    public ExceptionHttpServer(String desc) {
        super(desc);
    }
}
