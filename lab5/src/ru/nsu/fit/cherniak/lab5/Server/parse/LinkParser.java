package ru.nsu.fit.cherniak.lab5.Server.parse;

import ru.nsu.fit.cherniak.lab5.Stuff;

import java.util.HashMap;
import java.util.Map;

/**
 * Парсинт GET строки
 */
public class LinkParser {
    private String path;
    private Map<String, String> vars;

    /**
     * Конструктор
     *
     * @param link полная ссылка
     */
    public LinkParser(String link) {
        if (link.charAt(0) == '/') {
            link = link.substring(1, link.length());
        }
        String[] linkArr = link.split("[?]");
        path = linkArr[0];
        vars = new HashMap<>();
        if (linkArr.length == 2) {
            String[] varsParse = linkArr[1].split("&");
            String[] varParse;
            for (String aVarsParse : varsParse) {
                varParse = aVarsParse.split("=");
                vars.put(varParse[0], varParse.length == 2 ? Stuff.decodeURI(varParse[1]) : "");
            }
        }
    }

    /**
     * Возвращает путь
     *
     * @return Путь
     */
    public String getPath() {
        return path;
    }

    /**
     * Возвращает значение переменной
     * Если переменной нет, возвращается null
     *
     * @param value Имя еременной
     * @return Значение переменной
     */
    public String getValue(String value) {
        if (!vars.containsKey(value)) {
            return null;
        }
        return vars.get(value);
    }

    /**
     * Возвращает таблицу параметров
     *
     * @return Таблица
     */
    public Map<String, String> getMap() {
        return vars;
    }
}
