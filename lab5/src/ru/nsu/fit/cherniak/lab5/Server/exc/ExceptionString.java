package ru.nsu.fit.cherniak.lab5.Server.exc;

/**
 * Ошибка с пояснением
 * Может, такое уже есть, но мне лень искать)
 */
public class ExceptionString extends Exception {
    private String desc;

    public ExceptionString(String desc) {
        this.desc = desc;
    }

    public String get() {
        return this.desc;
    }
}
