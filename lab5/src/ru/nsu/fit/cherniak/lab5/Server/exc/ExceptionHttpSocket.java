package ru.nsu.fit.cherniak.lab5.Server.exc;

/**
 * Ошибка класса HttpSocket
 */
public class ExceptionHttpSocket extends ExceptionString {
    public ExceptionHttpSocket(String desc) {
        super(desc);
    }
}
