package ru.nsu.fit.cherniak.lab5.MatherServer;

import ru.nsu.fit.cherniak.lab5.Server.stuff.SocketEvent;

/**
 * For socket handlers, who work with server context
 */
public interface EnvironSocketEvent extends SocketEvent {
    void saveEnviron(ServerContext serverContext);
}
