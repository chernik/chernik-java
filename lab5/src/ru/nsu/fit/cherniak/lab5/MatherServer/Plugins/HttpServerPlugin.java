package ru.nsu.fit.cherniak.lab5.MatherServer.Plugins;

import ru.nsu.fit.cherniak.lab5.MatherServer.ServerContext;

public interface HttpServerPlugin {
    void initialize(ServerContext serverContext);

    String getName();
}
