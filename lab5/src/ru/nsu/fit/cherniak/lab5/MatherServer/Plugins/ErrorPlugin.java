package ru.nsu.fit.cherniak.lab5.MatherServer.Plugins;

import ru.nsu.fit.cherniak.lab5.MatherServer.ServerContext;

public class ErrorPlugin implements HttpServerPlugin {
    private String dataError;

    public String getPath() {
        return dataError;
    }

    public ErrorPlugin(String dataError) {
        this.dataError = dataError;
    }

    @Override
    public void initialize(ServerContext serverContext) {

    }

    @Override
    public String getName() {
        return "Standard error plugin";
    }
}
