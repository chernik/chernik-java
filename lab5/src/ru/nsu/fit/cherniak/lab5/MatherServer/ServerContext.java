package ru.nsu.fit.cherniak.lab5.MatherServer;

/**
 * Работа с настройками сервера
 * Используйте getServer(), чтобы получить сервер
 */
public interface ServerContext {
    /**
     * Получить сервер
     * @return Сервер
     */
    MatherHttpServer getServer();
}
