package ru.nsu.fit.cherniak.lab5.MatherServer.Mathers;

import java.util.function.Predicate;

public class IntMather extends PredicateMather {
    private static Predicate<String> predicate = (String str) -> {
        try {
            Integer.valueOf(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    };

    public IntMather(String name) {
        super(predicate, name);
    }
}
