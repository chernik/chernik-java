package ru.nsu.fit.cherniak.lab5.MatherServer.Interceptors;

import ru.nsu.fit.cherniak.lab5.Server.parse.HttpParser;
import ru.nsu.fit.cherniak.lab5.Server.stuff.HttpSender;

/**
 * Interceptor
 */
public interface Interceptor {
    /**
     * Interceptor
     *
     * @param httpParser Request
     * @param httpSender Response
     * @return true, if we should stop handler
     */
    boolean check(HttpParser httpParser, HttpSender httpSender);

    /**
     * Get name of interceptor, use for addAfter of ListInterceptors
     *
     * @return name
     */
    default String getName() {
        return "";
    }
}
