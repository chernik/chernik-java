package ru.nsu.fit.cherniak.lab5.MatherServer.Plugins;

public interface HttpServerCloseablePlugin extends HttpServerPlugin{
    void destroy();
}
