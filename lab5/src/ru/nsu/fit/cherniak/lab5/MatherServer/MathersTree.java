package ru.nsu.fit.cherniak.lab5.MatherServer;

import ru.nsu.fit.cherniak.lab5.MatherServer.Interceptors.UnorderedPipeLine;
import ru.nsu.fit.cherniak.lab5.MatherServer.Mathers.AnyMather;
import ru.nsu.fit.cherniak.lab5.MatherServer.Mathers.LastMather;
import ru.nsu.fit.cherniak.lab5.MatherServer.Mathers.SegmentMather;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class MathersTree {
    private List<MathersTree> next;
    private PointContainer point;
    private EnderContainer event;

    /**
     * Default constructor
     */
    MathersTree() {
        next = new LinkedList<>();
        point = new PointContainer();
        event = null;
    }

    /**
     * Find event in tree
     *
     * @param path   path
     * @param inters interceptors
     * @param map    filling map
     * @return constructor of event
     */
    public EnderContainer math(List<String> path, UnorderedPipeLine inters, Map<String, String> map) {
        return math(path, inters, map, 0);
    }

    synchronized private EnderContainer math(
            List<String> path, UnorderedPipeLine inters, Map<String, String> map, int ind) {
        point.getInterceptors(inters);
        if (point.getMather() != null) {
            int pos = point.getMather().math(path, map, ind);
            if (pos < 0) {
                return null;
            }
            ind += pos + 1;
        }
        if (ind == path.size()) {
            return this.event;
        }
        EnderContainer event;
        Map<String, String> bufMap = new HashMap<>();
        UnorderedPipeLine bufInters = new UnorderedPipeLine();
        for (MathersTree next : this.next) {
            bufMap.clear();
            bufInters.clear();
            event = next.math(path, bufInters, bufMap, ind);
            if (event != null) {
                bufMap.forEach(map::put);
                inters.addAll(bufInters);
                return event;
            }
        }
        return this.event;
    }

    /**
     * Add list of mathers & interceptors
     *
     * @param mathers mathers & interceptors
     * @param event   crator of event
     * @return true - sucs, false - never return, maybe)
     */
    public boolean registerPoints(List<PointContainer> mathers, EnderContainer event) {
        return register(mathers, event, -1);
    }

    /**
     * Add list of mathers in tree
     *
     * @param mathers mathers
     * @param event   creator of event
     * @return true - sucs, false - never return, maybe)
     */
    public boolean register(List<SegmentMather> mathers, EnderContainer event) {
        List<PointContainer> ms = mathers.stream()
                .map(PointContainer::new)
                .collect(Collectors.toList());
        return registerPoints(ms, event);
    }

    private boolean register(List<PointContainer> mathers, EnderContainer event, int ind) {
        if (ind >= 0 && point.getMather() != null) {
            if (!point.getMather().equals(mathers.get(ind).getMather())) {
                return false;
            }
        }
        if (ind >= 0 && point.getMather() == null) {
            point.intro(mathers.get(ind));
        }
        ind++;
        if (ind == mathers.size()) {
            this.event = event;
            return true;
        }
        for (MathersTree next : this.next) {
            if (next.register(mathers, event, ind)) {
                return true;
            }
        }
        MathersTree mathersTree = insertToEnd();
        return mathersTree.register(mathers, event, ind);
    }

    private MathersTree insertToEnd(){
        MathersTree mathersTree = new MathersTree();
        int i;
        i = next.size() - 1;
        while (i >= 0 && next.get(i).isLastMather()) {
            i--;
        }
        next.add(i + 1, mathersTree);
        return mathersTree;
    }

    /**
     * Registrate top of tree by interceptors
     * Abstract: that's not a tree, but element of tree
     *
     * @param inters interceptors
     */
    public void registerTop(UnorderedPipeLine inters) {
        point.intro(new PointContainer(point.getMather(), inters));
    }

    private boolean isLastMather(){
        return point.getMather() instanceof LastMather;
    }
}
