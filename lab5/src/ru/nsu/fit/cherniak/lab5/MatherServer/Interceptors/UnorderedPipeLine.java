package ru.nsu.fit.cherniak.lab5.MatherServer.Interceptors;

import java.util.ArrayList;
import java.util.List;

/**
 * Контейнер для интерсепторов для заполнения дерева
 */
public class UnorderedPipeLine {
    private List<Interceptor> inners;
    private List<Interceptor> outers;

    public UnorderedPipeLine(List<Interceptor> inners, List<Interceptor> outers) {
        this.inners = inners;
        this.outers = outers;
    }

    public UnorderedPipeLine(){
        inners = new ArrayList<>();
        outers = new ArrayList<>();
    }

    public UnorderedPipeLine(Interceptor inner){
        this();
        inners.add(inner);
    }

    public void addTo(PipeLine pipeLine){
        pipeLine.getInners().add(inners);
        pipeLine.getOuters().add(outers);
    }

    public void addAll(UnorderedPipeLine other){
        inners.addAll(other.inners);
        outers.addAll(other.outers);
    }

    public void clear(){
        inners.clear();
        outers.clear();
    }
}
