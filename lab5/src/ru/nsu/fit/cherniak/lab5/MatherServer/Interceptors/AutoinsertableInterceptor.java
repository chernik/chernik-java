package ru.nsu.fit.cherniak.lab5.MatherServer.Interceptors;

/**
 * Interceptor, who can inserts into pipe line automatically
 */
public interface AutoinsertableInterceptor extends Interceptor {
    void insert(ListInterceptors listInterceptors);
}
