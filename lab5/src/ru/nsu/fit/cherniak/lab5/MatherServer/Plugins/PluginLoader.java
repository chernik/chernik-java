package ru.nsu.fit.cherniak.lab5.MatherServer.Plugins;

import ru.nsu.fit.cherniak.lab5.Stuff;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 * Загрузчик плагинов из JAR
 */
public class PluginLoader {
    /**
     * Расположение файла базы плагинов
     */
    private static final String datafile = "plugins.txt";

    /**
     * Загрузить плагин из JAR, сохранив его в базу плагинов
     * Плагины на дублирование не проверяются
     *
     * @param f Файл JAR
     * @return Плагин
     * @throws PluginLoaderError Вылетает при любом удобном случае, проще посмотреть код этого метода
     */
    public static HttpServerPlugin loadPlugin(File f) throws PluginLoaderError {
        return loadPlugin(f, true);
    }

    private static HttpServerPlugin loadPlugin(File f, boolean isSave) throws PluginLoaderError {
        if (isSave) {
            savePlugin(f);
        }
        if (!f.exists() || f.isDirectory()) {
            throw new PluginLoaderError("Cannot find file");
        }
        if (!f.canRead()) {
            throw new PluginLoaderError("Cannot access to file");
        }
        Map<String, String> map = Stuff.Jar2Manifest(f);
        if (map == null) {
            throw new PluginLoaderError("Cannot get manifest");
        }
        String className = map.get("Plugin");
        if (className == null) {
            throw new PluginLoaderError("Cannot get 'Plugin' field contenting class name");
        }
        ClassLoader classLoader = Stuff.Jar2ClassLoader(f);
        if (classLoader == null) {
            throw new PluginLoaderError("Cannot create class loader");
        }
        Class<?> aClass;
        try {
            aClass = classLoader.loadClass(className);
        } catch (ClassNotFoundException e) {
            throw new PluginLoaderError("Class '" + className + "' not found");
        }
        Constructor<?> constructor;
        try {
            constructor = aClass.getConstructor();
        } catch (NoSuchMethodException e) {
            throw new PluginLoaderError("Class '" + className + "' don't contents 'constructor()'");
        }
        Object o;
        try {
            o = constructor.newInstance();
        } catch (InstantiationException e) {
            throw new PluginLoaderError("Cannot create abstract class");
        } catch (IllegalAccessException e) {
            throw new PluginLoaderError("Access error");
        } catch (InvocationTargetException e) {
            throw new PluginLoaderError("Catch exception from constructor\n" +
                    e.getTargetException().getMessage());
        }
        if (!(o instanceof HttpServerPlugin)) {
            throw new PluginLoaderError("Class '" + className + "' don't implements from 'HttpServerPlugin'");
        }
        return (HttpServerPlugin) o;
    }

    /**
     * Загрузить все плагины из базы плагинов
     * Непрогрузившиеся плагины будут заменены заглушкой ErrorPlugin
     * Его метод getPath() вернет путь к непрогрузившемуся плагину
     *
     * @return плагины
     */
    public static List<HttpServerPlugin> loadPlugins() {
        File f = new File(datafile);
        if (!f.exists() || f.isDirectory() || !f.canRead()) {
            return new ArrayList<>();
        }
        FileInputStream is;
        try {
            is = new FileInputStream(f);
        } catch (FileNotFoundException e) {
            return new ArrayList<>();
        }
        List<HttpServerPlugin> plugins = new Scanner(is).useDelimiter("[\r\n]")
                .tokens()
                .filter(s -> !s.equals(""))
                .map(path -> Paths.get(path).toFile())
                .map(file -> {
                    try {
                        return loadPlugin(file, false);
                    } catch (PluginLoaderError pluginLoaderError) {
                        return new ErrorPlugin("Load error from file<br><i>" +
                                file.getAbsolutePath() + "</i><br><b>" +
                                pluginLoaderError.get() + "</b>"
                        );
                    }
                })/*.filter(Objects::nonNull)*/.collect(Collectors.toList());
        try {
            is.close();
        } catch (IOException ignored) {
        }
        return plugins;
    }

    private static void savePlugin(File plugin) throws PluginLoaderError {
        File f = new File(datafile);
        if (!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException e) {
                throw new PluginLoaderError("Cannot create data file");
            }
        }
        if (f.isDirectory() || !f.canRead()) {
            throw new PluginLoaderError("Cannot save in data file");
        }
        FileOutputStream os;
        try {
            os = new FileOutputStream(f, true);
        } catch (FileNotFoundException e) {
            throw new PluginLoaderError("Cannot save in data file");
        }
        PrintStream ps = new PrintStream(os);
        ps.print(plugin.getAbsolutePath() + "\n");
        ps.close();
    }
}
