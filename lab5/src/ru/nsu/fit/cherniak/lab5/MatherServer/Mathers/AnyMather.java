package ru.nsu.fit.cherniak.lab5.MatherServer.Mathers;

import java.util.List;
import java.util.Map;

public class AnyMather implements LastMather {
    private String field;

    @Override
    public int hashCode() {
        return field == null ? 0 : field.hashCode();
    }

    @Override
    public boolean equals(Object other) {
        return other instanceof AnyMather &&
                other.hashCode() == this.hashCode() &&
                field != null &&
                this.field.equals(((AnyMather) other).field);
    }

    public AnyMather(String field) {
        this.field = field;
    }

    @Override
    public int math(List<String> path, Map<String, String> map, int ind) {
        if (field != null) {
            map.put(field, path.get(ind));
        }
        return 0;
    }
}
