package ru.nsu.fit.cherniak.lab5.MatherServer.Interceptors;

import ru.nsu.fit.cherniak.lab5.Server.parse.HttpParser;
import ru.nsu.fit.cherniak.lab5.Server.stuff.HttpSender;

import java.util.ArrayList;
import java.util.List;

/**
 * List of interceptors
 */
public class ListInterceptors {
    private List<Interceptor> interceptors;

    ListInterceptors() {
        interceptors = new ArrayList<>();
    }

    public ListInterceptors addFront(Interceptor interceptor) {
        interceptors.add(0, interceptor);
        return this;
    }

    public ListInterceptors addBack(Interceptor interceptor) {
        interceptors.add(interceptor);
        return this;
    }

    public ListInterceptors addAfter(Interceptor interceptor, String name) {
        int i;
        for (i = 0; i < interceptors.size(); i++) {
            if (interceptors.get(i).getName().equals(name)) {
                break;
            }
        }
        interceptors.add(i, interceptor);
        return this;
    }

    public void add(List<Interceptor> inters){
        inters.forEach(isr -> {
            if(isr instanceof AutoinsertableInterceptor){
                ((AutoinsertableInterceptor) isr).insert(this);
            } else {
                this.addBack(isr);
            }
        });
    }

    public boolean check(HttpParser httpParser, HttpSender httpSender) {
        for (Interceptor interceptor : interceptors) {
            if (interceptor.check(httpParser, httpSender)) {
                return true;
            }
        }
        return false;
    }

    public ListInterceptors getCopy(){
        ListInterceptors other = new ListInterceptors();
        //other.interceptors = interceptors.subList(0, interceptors.size());
        other.interceptors = new ArrayList<>();
        other.interceptors.addAll(interceptors);
        return other;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        interceptors.forEach(isr -> sb
                .append(isr.getName())
                .append("\n"));
        return sb.toString();
    }
}
