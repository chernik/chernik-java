package ru.nsu.fit.cherniak.lab5.MatherServer;

import ru.nsu.fit.cherniak.lab5.MatherServer.Interceptors.Interceptor;
import ru.nsu.fit.cherniak.lab5.MatherServer.Interceptors.UnorderedPipeLine;
import ru.nsu.fit.cherniak.lab5.MatherServer.Mathers.SegmentMather;
import ru.nsu.fit.cherniak.lab5.MatherServer.Plugins.HttpServerCloseablePlugin;
import ru.nsu.fit.cherniak.lab5.MatherServer.Plugins.HttpServerPlugin;
import ru.nsu.fit.cherniak.lab5.MatherServer.Plugins.PluginLoader;
import ru.nsu.fit.cherniak.lab5.Server.HttpServer;
import ru.nsu.fit.cherniak.lab5.Server.exc.ExceptionHttpServer;
import ru.nsu.fit.cherniak.lab5.Server.stuff.SocketEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class MatherHttpServer extends HttpServer {
    private MathersTree tree = new MathersTree();
    private List<HttpServerPlugin> plugins;
    private ServerContext serverContext;
    private boolean isFirstRun;

    /**
     * Using our socket event
     *
     * @param sizePool size of ThreadPool
     */
    public MatherHttpServer(int sizePool) {
        super(sizePool);
        isFirstRun = true;
        plugins = new ArrayList<>();
        MatherHttpServer s = this;
        serverContext = new ServerContext() {
            @Override
            public MatherHttpServer getServer() {
                return s;
            }
        };
        try {
            super.setEvent(() -> new MainMatherEvent(tree, serverContext));
        } catch (ExceptionHttpServer ignored) {
        }
    }

    /**
     * Registrate
     *
     * @param mathers
     * @param enderContainer
     */
    public void register(List<SegmentMather> mathers, EnderContainer enderContainer) {
        if (!tree.register(mathers, enderContainer)) {
            throw new ExceptionInInitializerError("WOW");
        }
    }

    public void registerPlugin(HttpServerPlugin httpServerPlugin) {
        plugins.add(httpServerPlugin);
    }

    public void registerPoints(List<PointContainer> points, EnderContainer enderContainer){
        tree.registerPoints(points, enderContainer);
    }

    public void registerTop(UnorderedPipeLine inters){
        tree.registerTop(inters);
    }

    /**
     * Override for block replacing socket event of our http server
     *
     * @param event Constructor of event
     * @throws ExceptionHttpServer Permanent error on excecute that method
     */
    @Override
    public void setEvent(Callable<SocketEvent> event) throws ExceptionHttpServer {
        throw new ExceptionHttpServer("Cannot set event in MatherHttpServer");
    }

    @Override
    public void start() throws ExceptionHttpServer {
        if (!isEnded()) {
            throw new ExceptionHttpServer("Server is running");
        }
        if(isFirstRun){
            isFirstRun = false;
            plugins.addAll(PluginLoader.loadPlugins());
        } else {
            plugins.stream()
                    .filter(obj -> obj instanceof HttpServerCloseablePlugin)
                    .map(obj -> (HttpServerCloseablePlugin)obj)
                    .forEach(HttpServerCloseablePlugin::destroy);
        }
        tree = new MathersTree();
        for (HttpServerPlugin plugin : plugins) {
            plugin.initialize(serverContext);
        }
        super.start();
    }

    private ServerContext getContext() {
        MatherHttpServer s = this;
        return new ServerContext() {
            @Override
            public MatherHttpServer getServer() {
                return s;
            }
        };
    }

    public List<HttpServerPlugin> getPlugins() {
        return plugins;
    }
}
