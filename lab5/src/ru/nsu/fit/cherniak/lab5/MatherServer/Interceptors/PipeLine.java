package ru.nsu.fit.cherniak.lab5.MatherServer.Interceptors;

public class PipeLine {
    private ListInterceptors inners;
    private ListInterceptors outers;

    public PipeLine() {
        inners = new ListInterceptors();
        outers = new ListInterceptors();
    }

    public ListInterceptors getInners() {
        return inners;
    }

    public ListInterceptors getOuters() {
        return outers;
    }

    public PipeLine getCopy(){
        PipeLine other = new PipeLine();
        other.inners = inners.getCopy();
        other.outers = outers.getCopy();
        return other;
    }

    public void add(UnorderedPipeLine inters){
        inters.addTo(this);
    }
}
