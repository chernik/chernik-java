package ru.nsu.fit.cherniak.lab5.MatherServer;

import ru.nsu.fit.cherniak.lab5.MatherServer.Interceptors.PipeLine;
import ru.nsu.fit.cherniak.lab5.MatherServer.Interceptors.UnorderedPipeLine;
import ru.nsu.fit.cherniak.lab5.Server.parse.HttpParser;
import ru.nsu.fit.cherniak.lab5.Server.stuff.HttpSender;
import ru.nsu.fit.cherniak.lab5.Server.stuff.SocketEvent;

import java.util.concurrent.Callable;

/**
 * Container for end of tree
 */
public class EnderContainer {
    private Callable<SocketEvent> ev;
    private PipeLine pipeLine;

    /**
     * Save only event
     *
     * @param ev event
     */
    public EnderContainer(Callable<SocketEvent> ev) {
        this.ev = ev;
        this.pipeLine = new PipeLine();
    }

    /**
     * Save event and nonull pipeline
     *
     * @param ev       event
     * @param pipeLine pipeline
     */
    public EnderContainer(Callable<SocketEvent> ev, PipeLine pipeLine) {
        if (ev == null || pipeLine == null) {
            throw new NullPointerException();
        }
        this.ev = ev;
        this.pipeLine = pipeLine;
    }

    /**
     * Execute handler
     *
     * @param httpParser    for handler
     * @param httpSender    for handler
     * @param inters        tree interceptors
     * @param serverContext for environ handler
     */
    public void run(HttpParser httpParser, HttpSender httpSender, UnorderedPipeLine inters, ServerContext serverContext) {
        PipeLine bufPipeLine = pipeLine.getCopy();
        bufPipeLine.add(inters);
        //test(bufPipeLine);
        if (!bufPipeLine.getInners().check(httpParser, httpSender)) {
            SocketEvent se;
            try {
                se = ev.call();
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
            if (se instanceof EnvironSocketEvent) {
                ((EnvironSocketEvent) se).saveEnviron(serverContext);
            }
            se.onSocket(httpParser, httpSender);
        }
        bufPipeLine.getOuters().check(httpParser, httpSender);
    }

    private void test(PipeLine pipeLine) {
        System.out.println("Inners>\n" +
                pipeLine.getInners().toString() +
                "Outers>\n" +
                pipeLine.getOuters().toString() +
                "----------------"
        );
    }
}
