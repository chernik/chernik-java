package ru.nsu.fit.cherniak.lab5.MatherServer.Mathers;

import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

public class PredicateMather implements SegmentMather {
    private Predicate<String> predicate;
    private String name;

    public PredicateMather(Predicate<String> predicate, String name) {
        this.predicate = predicate;
        this.name = name;
    }

    @Override
    public int hashCode() {
        return predicate.hashCode() + (name == null ? 0 : name.hashCode());
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof PredicateMather) {
            if (other.hashCode() != this.hashCode()) {
                return false;
            }
            if (!this.predicate.equals(((PredicateMather) other).predicate)) {
                return false;
            }
            if (this.name == null) {
                return ((PredicateMather) other).name == null;
            }
            return this.name.equals(((PredicateMather) other).name);
        }
        return false;
    }

    @Override
    public int math(List<String> path, Map<String, String> map, int ind) {
        String val = path.get(ind);
        if (predicate.test(val)) {
            if (name != null) {
                map.put(name, val);
            }
            return 0;
        }
        return -1;
    }
}
