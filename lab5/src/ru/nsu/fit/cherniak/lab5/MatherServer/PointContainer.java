package ru.nsu.fit.cherniak.lab5.MatherServer;

import ru.nsu.fit.cherniak.lab5.MatherServer.Interceptors.UnorderedPipeLine;
import ru.nsu.fit.cherniak.lab5.MatherServer.Mathers.SegmentMather;

public class PointContainer {
    private UnorderedPipeLine interceptors;
    private SegmentMather mather;

    public PointContainer() {
        mather = null;
        interceptors = new UnorderedPipeLine();
    }

    public PointContainer(SegmentMather s) {
        this();
        mather = s;
    }

    public PointContainer(SegmentMather s, UnorderedPipeLine lst) {
        this();
        mather = s;
        interceptors.addAll(lst);
    }

    public SegmentMather getMather() {
        return mather;
    }

    public void getInterceptors(UnorderedPipeLine list) {
        list.addAll(interceptors);
    }

    public void intro(PointContainer other) {
        mather = other.mather;
        interceptors.addAll(other.interceptors);
    }
}
