package ru.nsu.fit.cherniak.lab5.MatherServer;

import ru.nsu.fit.cherniak.lab5.MatherServer.Interceptors.PipeLine;
import ru.nsu.fit.cherniak.lab5.MatherServer.Interceptors.UnorderedPipeLine;
import ru.nsu.fit.cherniak.lab5.Server.exc.ExceptionHttpParser;
import ru.nsu.fit.cherniak.lab5.Server.parse.HttpParser;
import ru.nsu.fit.cherniak.lab5.Server.parse.LinkParser;
import ru.nsu.fit.cherniak.lab5.Server.stuff.HttpSender;
import ru.nsu.fit.cherniak.lab5.Server.stuff.SocketEvent;

import java.util.*;

/**
 * Event for MatherHttpServer
 */
class MainMatherEvent implements SocketEvent {
    private MathersTree tree;
    private ServerContext serverContext;

    MainMatherEvent(MathersTree tree, ServerContext serverContext) {
        this.tree = tree;
        this.serverContext = serverContext;
    }

    @Override
    public void onSocket(HttpParser httpParser, HttpSender httpSender) {
        String link;
        try {
            link = httpParser.getLink();
        } catch (ExceptionHttpParser exceptionHttpParser) {
            httpSender.sendError("Cannot get link", 405);
            return;
        }
        LinkParser linkParser = new LinkParser(link);
        httpParser.setLinkParser(linkParser);
        String path = linkParser.getPath();
        List<String> list = new LinkedList<>();
        list.addAll(Arrays.asList(path.split("/", -1)));
        Map<String, String> map = new HashMap<>();
        UnorderedPipeLine inters = new UnorderedPipeLine();
        EnderContainer res = tree.math(list, inters, map);
        if (res == null) {
            httpSender.sendError(404);
            return;
        }
        httpParser.setPathInfo(map);
        res.run(httpParser, httpSender, inters, serverContext);
    }
}
