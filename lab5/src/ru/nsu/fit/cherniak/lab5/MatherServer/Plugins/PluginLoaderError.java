package ru.nsu.fit.cherniak.lab5.MatherServer.Plugins;

import ru.nsu.fit.cherniak.lab5.Server.exc.ExceptionString;

public class PluginLoaderError extends ExceptionString {
    public PluginLoaderError(String desc) {
        super(desc);
    }
}
