package ru.nsu.fit.cherniak.lab5;

import ru.nsu.fit.cherniak.lab5.Server.stuff.MultipartContainer;

import javax.xml.crypto.dsig.Manifest;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLDecoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.jar.Attributes;
import java.util.jar.JarInputStream;

/**
 * Полезные функции
 */
public class Stuff {

    public final static String byteCharset = "Cp1252";

    /**
     * Перевод таблицы в формат
     * $key -> $value$delim
     *
     * @param map   таблица
     * @param delim разделитель
     */
    public static String getViewOfMap(Map<String, String> map, String delim) {
        StringBuilder sb = new StringBuilder();
        map.forEach((String key, String value) ->
                sb.append(key).append(" -> ").append(value).append(delim));
        return sb.toString();
    }

    /**
     * Вывод multipart в виде
     * <h3>Part <i>$name</i> with type <i>$mime</i></h3>
     * $name -> $value(параметры)<br>
     * <hr>$data<br><hr>
     *
     * @param map таблица name -> part
     * @return строковое представление
     */
    public static String getViewOfMultipart(Map<String, MultipartContainer> map) {
        StringBuilder sb = new StringBuilder();
        map.forEach((String key, MultipartContainer value) ->
                sb.append("<h3>Part <i>").append(key).append("</i> with type <i>")
                        .append(value.getMimeType()).append("</i> ></h3>")
                        .append(getViewOfMap(value.getMap(), "<br>"))
                        .append("<hr>").append(new String(value.getData())).append("<br><hr>"));
        return sb.toString();
    }

    /**
     * Соеденить два байтовых массива
     *
     * @param one первый массив
     * @param two второй массив
     * @return результат
     */
    public static byte[] concatByte(byte[] one, byte[] two) {
        byte[] res = new byte[one.length + two.length];
        System.arraycopy(one, 0, res, 0, one.length);
        System.arraycopy(two, 0, res, one.length, two.length);
        return res;
    }

    /**
     * Удалить обрамляющие скобки
     *
     * @param str строка
     * @return результат
     */
    public static String delQuotes(String str) {
        if (
                str != null
                        && str.length() >= 2
                        && str.charAt(0) == '"'
                        && str.charAt(str.length() - 1) == '"'
                ) {
            return str.substring(1, str.length() - 1);
        } else {
            return str;
        }
    }

    /**
     * Взять байты (окончание на \n или на \r\n) из потока
     *
     * @param is входной поток
     * @return байты
     */
    public static byte[] getLineRNFromStream(BufferedInputStream is) throws IOException {
        List<Byte> arr = new ArrayList<>();
        int b;
        while (true) {
            b = is.read();
            if (b < 0 || b > 255) {
                throw new IOException();
            }
            arr.add((byte) b);
            if (b == '\n') {
                arr.remove(arr.size() - 1);
                if (arr.get(arr.size() - 1) == '\r') {
                    arr.remove(arr.size() - 1);
                }
                byte[] bArr = new byte[arr.size()];
                for (int i = 0; i < arr.size(); i++) {
                    bArr[i] = arr.get(i);
                }
                return bArr;
            }
        }
    }

    /**
     * Декодирует %2F
     *
     * @param str Исходная строка
     * @return Результат
     */
    public static String decodeURI(String str) {
        try {
            return URLDecoder.decode(str, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return str;
        }
    }

    /**
     * Берем md5 от строки
     *
     * @param str Строка
     * @return md5
     * @throws NoSuchAlgorithmException Если нету такого алгоритма
     */
    public static String getMD5(String str) throws NoSuchAlgorithmException {
        MessageDigest m = MessageDigest.getInstance("MD5");
        m.update(str.getBytes());
        StringBuilder sb = new StringBuilder();
        for (byte b : m.digest()) {
            sb.append(Integer.toHexString(0xff & b));
        }
        return sb.toString();
    }

    /**
     * Берем поля манифеста из JAR
     *
     * @param f Файл JAR
     * @return Поля Ключ: значение
     */
    public static Map<String, String> Jar2Manifest(File f) {
        JarInputStream jarInputStream;
        try {
            jarInputStream = new JarInputStream(new FileInputStream(f));
        } catch (IOException e) {
            return null;
        }
        Map<String, String> map = new HashMap<>();
        java.util.jar.Manifest manifest = jarInputStream.getManifest();
        if(manifest == null){
            return null;
        }
        manifest.getMainAttributes().forEach((a, b) -> map.put(
                a.toString(),
                b.toString()
        ));
        try {
            jarInputStream.close();
        } catch (IOException ignored) {
        }
        return map;
    }

    /**
     * Берем загрузчик классов из JAR
     *
     * @param f Файл JAR
     * @return Загрузчик
     */
    public static ClassLoader Jar2ClassLoader(File f) {
        URL url;
        try {
            url = f.toURI().toURL();
        } catch (MalformedURLException e) {
            return null;
        }
        return new URLClassLoader(new URL[]{url});
    }

    public static String html2text(String txt){
        return "<pre><code>" + txt.replace("<", "&lt;")
                .replace(">", "&gt;") + "</code></pre>";
    }
}
