package ru.nsu.fit.cherniak.lab5.TestPlugin;

import ru.nsu.fit.cherniak.lab5.MatherServer.EnderContainer;
import ru.nsu.fit.cherniak.lab5.MatherServer.Mathers.StringMather;
import ru.nsu.fit.cherniak.lab5.MatherServer.Plugins.HttpServerPlugin;
import ru.nsu.fit.cherniak.lab5.MatherServer.ServerContext;

import java.util.Arrays;
import java.util.Collections;

public class TestPlugin implements HttpServerPlugin {
    @Override
    public void initialize(ServerContext serverContext) {
        serverContext.getServer().register(Collections.singletonList(
                new StringMather("test", null)
        ), new EnderContainer(
                TestSocketEvent::new
        ));
    }

    @Override
    public String getName() {
        return "Test plugin";
    }
}
