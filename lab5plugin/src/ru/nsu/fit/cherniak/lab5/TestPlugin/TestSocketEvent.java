package ru.nsu.fit.cherniak.lab5.TestPlugin;

import ru.nsu.fit.cherniak.lab5.Server.parse.HttpParser;
import ru.nsu.fit.cherniak.lab5.Server.stuff.HttpSender;
import ru.nsu.fit.cherniak.lab5.Server.stuff.SocketEvent;

public class TestSocketEvent implements SocketEvent {
    @Override
    public void onSocket(HttpParser httpParser, HttpSender httpSender) {
        httpSender.sendData("Test plugin works!");
    }
}
