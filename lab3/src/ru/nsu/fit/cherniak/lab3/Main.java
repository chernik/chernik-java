package ru.nsu.fit.cherniak.lab3;

import ru.nsu.fit.cherniak.lab3.ThreadPool.Promise;
import ru.nsu.fit.cherniak.lab3.ThreadPool.ThreadPool;

import java.util.concurrent.atomic.AtomicInteger;

public class Main {
    private static void test1() throws InterruptedException {
        System.err.println("Create pool...");
        ThreadPool thp = new ThreadPool(4);
        System.err.println("ThreadPool started!\n-----------");
        Thread.sleep(1000);
        System.err.println("Adding task...");
        Promise p = thp.addTask(() -> {
            Thread.sleep(1000);
            System.err.println(">>>>>>>Test task");
            return "Hi";
        });
        System.err.println("Tasks added!\n--------");
        Thread.sleep(2000);
        System.err.print("Wait...");
        while(!p.isReady()){
            System.err.print("\rWait...");
        }
        System.err.println("Sucs!\n-------------");
        Thread.sleep(1000);
        System.err.println("Joining...");
        thp.join();
        System.err.println("Joined!\n----------");
    }

    private static int test2() throws InterruptedException {
        ThreadPool thp = new ThreadPool(4);
        AtomicInteger k = new AtomicInteger();
        int i;
        System.err.println("--------");
        for(i = 0; i < 1000000; i++) {
            if((i % 1000) == 0) {
                System.err.print("\r" + i);
            }
            thp.addTask(() -> {
                synchronized (k) {
                    k.getAndIncrement();
                }
                return null;
            });
        }
        System.err.println("\nWait...");
        Thread.sleep(1000);
        thp.join();
        //Thread.sleep(1000);
        int kkTest = k.get();
        System.err.println("-----------\nALL " + kkTest);
        System.err.println(kkTest == 1000000 ? "RIGHT" : "WRONG");
        return kkTest;
    }

    public static void main(String[] args) throws InterruptedException {
        int arr[] = new int[10];
        for(int i = 0; i < arr.length; i++){
            arr[i] = test2();
        }
        for(int i = 0; i < arr.length; i++){
            System.err.print(arr[i] == 1000000 ? 1 : 0);
        }
        System.err.println();
    }
}
