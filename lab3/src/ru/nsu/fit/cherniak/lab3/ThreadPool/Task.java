package ru.nsu.fit.cherniak.lab3.ThreadPool;

import java.util.concurrent.Callable;

public class Task {
    private final Promise promise;
    private final Callable<Object> cb;

    /**
     * Constructor of Task
     * @param cb function
     */
    public Task(Callable<Object> cb){
        promise = new Promise(cb);
        this.cb = cb;
    }

    /**
     * get promise
     * @return promise
     */
    public Promise getPromise() {
        return promise;
    }

    /**
     * Run function
     */
    public void run(){
        if(promise.isReady()){
            return;
        }
        Object o = null;
        try {
            o = cb.call(); // time
        } catch (Exception e) {
            promise.setError(e);
            return;
        }
        promise.setSucs(o);
    }


}
