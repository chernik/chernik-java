package ru.nsu.fit.cherniak.lab3.ThreadPool;

public class Syncro {
    private final Object async;
    private int k;

    public Syncro() {
        this.async = new Object();
        k = 0;
    }

    public void waitS(){
        synchronized (async) {
            k++;
            if(k <= 0){
                return;
            }
            while (true) {
                try {
                    async.wait();
                } catch (InterruptedException e) {
                    continue;
                }
                break;
            }
        }
    }

    public void notifyS(){
        synchronized (async){
            k--;
            async.notify();
        }
    }
}
