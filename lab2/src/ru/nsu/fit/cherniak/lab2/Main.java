package ru.nsu.fit.cherniak.lab2;

import ru.nsu.fit.cherniak.lab2.GUI.GUIFrame;

public class Main {
    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(GUIFrame::new);
    }
}
