package ru.nsu.fit.cherniak.lab2.GUI;

import ru.nsu.fit.cherniak.lab2.MySocketEvent;
import ru.nsu.fit.cherniak.lab2.Server.HttpServer;
import ru.nsu.fit.cherniak.lab2.Server.exc.ExceptionHttpServer;

import javax.swing.*;
import java.awt.*;

/**
 * Класс для гуишного окна
 */
public class GUIFrame extends JFrame {
    private JLabel log;
    private JButton butStart, butStop;

    private HttpServer server;

    /**
     * Конструктор окна
     */
    public GUIFrame() {
        server = new HttpServer(new MySocketEvent());
        start();
    }

    /**
     * Установка элементов окна
     */
    private void start() {
        //Dimension size = new Dimension(100, 30);
        // Окно
        setDefaultLookAndFeelDecorated(true);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        setPreferredSize(new Dimension(300, 500));
        setAlwaysOnTop(true);
        setVisible(true);
        pack();
        addWindowListener(new CloseFrameEvent(this, server));
        setLocationRelativeTo(null);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        screenSize.width -= getWidth();
        screenSize.height -= getHeight() + 50;
        setLocation(screenSize.width, screenSize.height);
        // Панель логгинга
        log = new JLabel();
        log.setText("<empty>");
        //log.setPreferredSize(size);
        //log.setHorizontalAlignment(JLabel.LEFT);
        //log.setVerticalAlignment(JLabel.TOP);
        add(log, BorderLayout.NORTH);
        // Таймер для логгинга
        Timer tm = new Timer(100, e -> tmEvent());
        // Кнопка старта сервера
        butStart = new JButton();
        butStart.setText("Start");
        //butStart.setPreferredSize(size);
        //butStart.setHorizontalAlignment(JLabel.LEFT);
        //butStart.setVerticalAlignment(JLabel.TOP);
        //butStart.setLocation(0,50);
        butStart.addMouseListener((EventClick) this::butStartEvent);
        add(butStart, BorderLayout.CENTER);
        // Кнопка остановки сервера
        butStop = new JButton();
        butStop.setText("Stop");
        //butStop.setPreferredSize(size);
        //butStop.setHorizontalAlignment(JLabel.LEFT);
        //butStop.setVerticalAlignment(JLabel.TOP);
        butStop.addMouseListener((EventClick) this::butStopEvent);
        add(butStop, BorderLayout.SOUTH);
        // Запуск элементов
        tm.start();
        //butStartEvent();
    }

    /**
     * Обработчик таймера
     * Выполняет лог сервера
     */
    private void tmEvent() {
        String state = server.getState();
        if (state.equals("")) {
            state = "<empty>";
        }
        log.setText(state);
        if (server.isEnded()) {
            butStop.setEnabled(false);
            butStart.setEnabled(true);
        } else {
            butStop.setEnabled(true);
            butStart.setEnabled(false);
        }
    }

    /**
     * Обработчик кнопки запуска сервера
     */
    private void butStartEvent() {
        if (!butStart.isEnabled()) {
            return;
        }
        try {
            server.start();
        } catch (ExceptionHttpServer exceptionHttpServer) {
            log.setText("ERROR > at start");
        }
    }

    /**
     * Обработчик кнопки остановки сервера
     */
    private void butStopEvent() {
        if (!butStop.isEnabled()) {
            return;
        }
        try {
            server.stop();
        } catch (ExceptionHttpServer exceptionHttpServer) {
            log.setText("ERROR > stop");
        }
    }
}
