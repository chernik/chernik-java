package ru.nsu.fit.cherniak.lab2;

import ru.nsu.fit.cherniak.lab2.Server.stuff.MultipartContainer;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Полезные функции
 */
public class Stuff {

    public final static String byteCharset = "Cp1252";

    /**
     * Перевод таблицы в формат
     * $key -> $value$delim
     *
     * @param map   таблица
     * @param delim разделитель
     */
    public static String getViewOfMap(Map<String, String> map, String delim) {
        StringBuilder sb = new StringBuilder();
        map.forEach((String key, String value) ->
                sb.append(key).append(" -> ").append(value).append(delim));
        return sb.toString();
    }

    /**
     * Вывод multipart в виде
     * <h3>Part <i>$name</i> with type <i>$mime</i></h3>
     * $name -> $value(параметры)<br>
     * <hr>$data<br><hr>
     *
     * @param map таблица name -> part
     * @return строковое представление
     */
    public static String getViewOfMultipart(Map<String, MultipartContainer> map) {
        StringBuilder sb = new StringBuilder();
        map.forEach((String key, MultipartContainer value) ->
                sb.append("<h3>Part <i>").append(key).append("</i> with type <i>")
                        .append(value.getMimeType()).append("</i> ></h3>")
                        .append(getViewOfMap(value.getMap(), "<br>"))
                        .append("<hr>").append(new String(value.getData())).append("<br><hr>"));
        return sb.toString();
    }

    /**
     * Соеденить два байтовых массива
     *
     * @param one первый массив
     * @param two второй массив
     * @return результат
     */
    public static byte[] concatByte(byte[] one, byte[] two) {
        byte[] res = new byte[one.length + two.length];
        System.arraycopy(one, 0, res, 0, one.length);
        System.arraycopy(two, 0, res, one.length, two.length);
        return res;
    }

    /**
     * Удалить обрамляющие скобки
     *
     * @param str строка
     * @return результат
     */
    public static String delQuotes(String str) {
        if (
                str != null
                        && str.length() >= 2
                        && str.charAt(0) == '"'
                        && str.charAt(str.length() - 1) == '"'
                ) {
            return str.substring(1, str.length() - 1);
        } else {
            return str;
        }
    }

    /**
     * Взять байты (окончание на \n или на \r\n) из потока
     *
     * @param is входной поток
     * @return байты
     */
    public static byte[] getLineRNFromStream(BufferedInputStream is) throws IOException {
        List<Byte> arr = new ArrayList<>();
        int b;
        while (true) {
            b = is.read();
            if (b < 0 || b > 255) {
                throw new IOException();
            }
            arr.add((byte) b);
            if (b == '\n') {
                arr.remove(arr.size() - 1);
                if (arr.get(arr.size() - 1) == '\r') {
                    arr.remove(arr.size() - 1);
                }
                byte[] bArr = new byte[arr.size()];
                for (int i = 0; i < arr.size(); i++) {
                    bArr[i] = arr.get(i);
                }
                return bArr;
            }
        }
    }

    /**
     * Декодирует %2F
     *
     * @param str Исходная строка
     * @return Результат
     */
    public static String decodeURI(String str) {
        try {
            return URLDecoder.decode(str, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return str;
        }
    }

}
