package ru.nsu.fit.cherniak.lab2.Server;

import ru.nsu.fit.cherniak.lab2.Server.stuff.HttpSender;
import ru.nsu.fit.cherniak.lab2.Server.stuff.LogThread;
import ru.nsu.fit.cherniak.lab2.Server.stuff.SocketEvent;
import ru.nsu.fit.cherniak.lab2.Server.exc.ExceptionHttpSender;
import ru.nsu.fit.cherniak.lab2.Server.exc.ExceptionHttpSocket;
import ru.nsu.fit.cherniak.lab2.Server.parse.HttpParser;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.net.Socket;
import java.net.SocketException;

/**
 * Работа с отдельным HTTP/1.1 запросом
 */
public class HttpSocket extends LogThread {
    private Socket s; // Собсно сокет
    private InputStream is; // Входной поток
    private OutputStream os; // Выходной поток
    private int timeout; // Максимальное время работы с сокетом
    private SocketEvent event;

    /**
     * Установка нового максимального времени ожидания данных
     *
     * @param timeout время в миллисекундах
     */
    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    /**
     * Конструктор
     *
     * @param s     сокет
     * @param event обработчик пришедших данных
     * @throws ExceptionHttpSocket ошибка сокета
     */
    public HttpSocket(Socket s, SocketEvent event) throws ExceptionHttpSocket {
        this.event = event;
        timeout = 5000;
        try {
            s.setSoTimeout(timeout);
        } catch (SocketException e) {
            throw new ExceptionHttpSocket("cannot set timeout");
        }
        this.s = s;
        try {
            is = s.getInputStream();
        } catch (IOException e) {
            throw new ExceptionHttpSocket("no input stream");
        }
        try {
            os = s.getOutputStream();
        } catch (IOException e) {
            throw new ExceptionHttpSocket("no output stream");
        }
    }

    /**
     * Запуск потока сокета
     */
    public void start() {
        super.getThread(() -> {
            setState("started");
            try {
                run();
            } catch (UncheckedIOException err) {
                setError("read timeout");
                return;
            }
            try {
                os.flush();
            } catch (IOException e) {
                setError("cannot flush");
            }
            try {
                s.close();
            } catch (IOException e) {
                setError("cannot send");
            }
        }).start();
    }

    /**
     * Нормальная обработка сокета
     */
    private void run() {
        HttpParser httpParser = new HttpParser(is);
        HttpSender httpSender = new HttpSender(os);
        setState("start event...");
        event.onSocket(httpParser, httpSender);
        if (!httpSender.isSended()) {
            try {
                httpSender.goAway();
            } catch (ExceptionHttpSender exceptionHttpSender) {
                setError("ERROR > " + exceptionHttpSender.get());
                return;
            }
        }
        setState("event finished");
    }
}
