package ru.nsu.fit.cherniak.lab2.Server.stuff;

import ru.nsu.fit.cherniak.lab2.Server.parse.HttpParser;

public interface SocketEvent {
    void onSocket(HttpParser httpParser, HttpSender httpSender);
}
