package ru.nsu.fit.cherniak.lab2.Server;

import ru.nsu.fit.cherniak.lab2.Server.stuff.LogThread;
import ru.nsu.fit.cherniak.lab2.Server.stuff.SocketEvent;
import ru.nsu.fit.cherniak.lab2.Server.exc.ExceptionHttpServer;
import ru.nsu.fit.cherniak.lab2.Server.exc.ExceptionHttpSocket;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Сервер в отдельном потоке с логгированием
 */
public class HttpServer extends LogThread {
    private int port; // Порт
    private boolean isStopping; // Для остановки потока
    private HttpSocket hs; // Сокет
    private SocketEvent event; // Обработчик сокета

    /**
     * Установка нового порта
     *
     * @param port порт
     */
    public void setPort(int port) {
        this.port = port;
    }

    /**
     * Стандартный конструктор
     */
    public HttpServer(SocketEvent event) {
        port = 8080;
        isStopping = false;
        hs = null;
        this.event = event;
    }

    /**
     * Запуск сервера
     * Невозможен при уже запущенном сервере
     */
    public void start() throws ExceptionHttpServer {
        if (!super.isEnded()) {
            throw new ExceptionHttpServer("Server already run");
        }
        isStopping = false;
        setState("starting...");
        super.getThread(this::startServer).start(); // Запускаем сервер
    }

    /**
     * Остановка сервера
     * Невозможна при уже остановленном сервере
     * Сервер может сразу не остановиться, необходимо следить либо за состоянием (must be 'stopped') либо isStopped()
     */
    public void stop() throws ExceptionHttpServer {
        if (super.isEnded() || isStopping) {
            throw new ExceptionHttpServer("Already stopped or stopping");
        }
        isStopping = true;
        setState("stopping...");
    }

    /**
     * Поток сервера
     */
    private void startServer() {
        ServerSocket ss;
        try {
            ss = new ServerSocket(port);
        } catch (IOException e) {
            setError("no open port");
            return;
        }
        Socket s;
        while (!isStopping) {
            setState("waiting...");
            try {
                s = ss.accept();
            } catch (IOException e) {
                setError("cannot get socket");
                continue;
            }
            setState("catched");
            startSocket(s);
        }
        try {
            ss.close();
        } catch (IOException e) {
            setError("cannot close server");
        }
        setState("stopped");
    }

    /**
     * Обработка запроса
     */
    private void startSocket(Socket s) {
        try {
            hs = new HttpSocket(s, event);
        } catch (ExceptionHttpSocket exceptionHttpSocket) {
            setError("socket error > " + exceptionHttpSocket.get());
            return;
        }
        setState("doing socket...");
        hs.start();
        // hs = null;
    }

    /**
     * Взять текущее состояние
     * Если есть сокет, то добавить состояние сокета
     *
     * @return состояние
     */
    @Override
    public String getState() {
        if (hs == null) {
            return super.getState();
        }
        return super.getState() + " > " + hs.getState();
    }
}
