package ru.nsu.fit.cherniak.lab2.Server.stuff;

import ru.nsu.fit.cherniak.lab2.Server.exc.ExceptionHttpSender;
import ru.nsu.fit.cherniak.lab2.Stuff;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

public class HttpSender {
    private static final int codeSucs = 200; // Код успеха
    private static final String typeHtml = "text/html"; // Тип HTML текста

    private byte[] answer; // Ответ
    private OutputStream os; // Поток вывода
    private Map<String, String> headPars; // Параметры заголовка
    private String version; // Версия HTTP протокола
    private Map<Integer, String> codeErr; // Коды ошибок
    private boolean isSend; // Был ли ответ отправлен

    /**
     * Отправляли ли уже запрос
     *
     * @return true - отправляли
     */
    public boolean isSended() {
        return isSend;
    }

    /**
     * Установить параметр в заголовок ответа
     * Изменение параметров Content-Type и Content-Length не влияет на их реальные значения
     *
     * @param name  имя параметра
     * @param value значение
     */
    public void setVar(String name, String value) {
        if (headPars.containsKey(name)) {
            headPars.replace(name, value);
        } else {
            headPars.put(name, value);
        }
    }

    /**
     * @param os ожидается Socket.getOutputStream
     */
    public HttpSender(OutputStream os) {
        this.os = os;
        headPars = new HashMap<>();
        headPars.put("Content-Type", typeHtml);
        headPars.put("Content-Length", "0");
        headPars.put("Connection", "close");
        version = "HTTP/1.1";
        codeErr = new HashMap<>();
        codeErr.put(200, "OK");
        answer = null;
        isSend = false;
    }

    /**
     * Отправка ошибки с определенным кодом
     *
     * @param code код
     */
    public void sendError(int code) {
        sendError(getDesc(code), code);
    }

    /**
     * Отправка ошибки с прилагающимся текстом
     *
     * @param text текст
     * @param code код ошибки
     */
    public void sendError(String text, int code) {
        String str = "<b><center style=\"font-size:40\">" +
                code + " " + text +
                "</center></b><br><hr>";
        sendString(str, code);
    }

    /**
     * Отправка ошибки с прилагающимся произвольным телом
     *
     * @param data тело
     * @param type тип тела
     * @param code код ошибки
     */
    public void sendError(byte[] data, String type, int code) {
        sendBytes(data, type, code);
    }

    /**
     * Отправка строки
     *
     * @param data строка
     */
    public void sendData(String data) {
        sendString(data, codeSucs);
    }

    /**
     * Отправка произвольных данных
     *
     * @param data данные
     * @param type тип данных
     */
    public void sendData(byte[] data, String type) {
        sendBytes(data, type, codeSucs);
    }

    public void goAway() throws ExceptionHttpSender {
        if (answer == null) {
            throw new ExceptionHttpSender("empty answer");
        }
        if (isSended()) {
            throw new ExceptionHttpSender("already sended");
        }
        try {
            os.write(answer);
            isSend = true;
        } catch (IOException e) {
            throw new ExceptionHttpSender("write error");
        }
    }

    private void sendString(String txt, int code) {
        if (txt == null) {
            txt = "";
        }
        byte[] data;
        //try {
        data = txt.getBytes(Charset.defaultCharset());
        //} catch (UnsupportedEncodingException e) {
        //    data = txt.getBytes();
        //}
        sendBytes(data, typeHtml, code);
    }

    private void sendBytes(byte[] data, String type, int code) {
        setVar("Content-Type", type);
        setVar("Content-Length", String.valueOf(data.length));
        String head = getHead(code);
        sendFinal(Stuff.concatByte(
                head.getBytes(),
                data
        ));
    }

    private void sendFinal(byte[] data) {
        answer = data;
    }

    private String getHead(int code) {
        String enter = "\r\n";
        StringBuilder sb = new StringBuilder();
        sb.append(version).append(" ").append(code).append(" ").append(getDesc(code)).append(enter);
        headPars.forEach((String name, String value) -> sb.append(name).append(": ").append(value).append(enter));
        sb.append(enter);
        return sb.toString();
    }

    private String getDesc(int code) {
        if (!codeErr.containsKey(code)) {
            return "Unknown";
        }
        return codeErr.get(code);
    }
}
