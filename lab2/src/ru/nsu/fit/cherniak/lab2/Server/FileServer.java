package ru.nsu.fit.cherniak.lab2.Server;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Файловый сервер
 * Позволяет находить файлы по пути и загружать их
 */
public class FileServer {
    private String path;
    private String defName;

    public FileServer() {
        this.path = "static"; // По ТЗ
        this.defName = "index.html"; // По ТЗ
    }

    /**
     * Ищет файл по пути
     *
     * @param path Путь
     * @return Существующий файл либо null в случае ошибки
     */
    public File getFile(String path) {
        if (path.endsWith("/")) { // По ТЗ
            return null;
        }
        File f;
        String fullPath = this.path + (this.path.length() == 0 ? "" : "/") + path;
        f = Paths.get(fullPath).toFile();
        if (f.exists() && f.isFile() && f.canRead()) {
            return f;
        }
        f = Paths.get(fullPath + (fullPath.length() == 0 ? "" : "/") + defName).toFile();
        if (!f.exists() || !f.isFile() || !f.canRead()) {
            return null;
        }
        return f;
    }

    /**
     * Взять бинарный файл
     *
     * @param f Файл
     * @return строка на основе массива байт
     */
    public byte[] loadBinary(File f) {
        FileInputStream fr;
        try {
            fr = new FileInputStream(f);
        } catch (FileNotFoundException e) {
            return null;
        }
        byte[] arr;
        try {
            arr = fr.readAllBytes();
        } catch (IOException e1) {
            arr = null;
        }
        try {
            fr.close();
        } catch (IOException ignored) {

        }
        return arr;
    }

    public String getMimeType(File f) {
        String mime;
        try {
            mime = Files.probeContentType(Paths.get(f.getPath()));
        } catch (IOException e) {
            mime = "application/binary";
        }
        return mime;
    }

    /**
     * Установка нового пути для сервера
     *
     * @param path Новый путь
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * Установка нового имени по умолчанию
     *
     * @param defName Новое имя
     */
    public void setDefName(String defName) {
        this.defName = defName;
    }
}
