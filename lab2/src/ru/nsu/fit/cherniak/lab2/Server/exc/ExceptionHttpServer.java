package ru.nsu.fit.cherniak.lab2.Server.exc;

/**
 * Ошибка класса HttpServer
 */
public class ExceptionHttpServer extends ExceptionString {
    public ExceptionHttpServer(String desc) {
        super(desc);
    }
}
