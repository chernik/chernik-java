package ru.nsu.fit.cherniak.lab2.Server.stuff;

import ru.nsu.fit.cherniak.lab2.Server.exc.ExceptionLogThread;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Класс для потоков с передачей состояния
 */
public class LogThread {
    private Thread th; // Поток
    private String state; // Состояние
    private AtomicBoolean isError; // Есть ли ошибка
    private AtomicBoolean isRun; // Выполняется ли сейчас
    private final Object mutex;

    /**
     * Установка начальных значений
     */
    public LogThread() {
        th = null;
        state = "";
        isError = new AtomicBoolean(false);
        isRun = new AtomicBoolean(false);
        mutex = new Object();
    }

    /**
     * Создает поток с установкой нужных полей
     *
     * @param main Команды потока
     * @return Поток
     */
    public Thread getThread(Runnable main) {
        th = new Thread(() -> {
            synchronized (mutex) {
                isError.set(false);
                isRun.set(true);
                state = "running";
            }
            main.run();
            synchronized (mutex) {
                if (!isError.get()) {
                    state = "ended sucs";
                }
                isRun.set(false);
            }
        });
        return th;
    }

    public String getState() {
        boolean err;
        synchronized (mutex) {
            err = isError.get();
        }
        if (err) {
            return "ERROR " + state;
        }
        return state;
    }

    public void setState(String state) {
        synchronized (mutex) {
            this.state = state;
        }
    }

    public void setError(String desc) {
        synchronized (mutex) {
            state = desc;
            isError.set(true);
        }
    }

    public boolean hasError() {
        boolean err;
        synchronized (mutex) {
            err = isError.get();
            isError.set(false);
        }
        return err;
    }

    public boolean isEnded() {
        synchronized (mutex) {
            return !isRun.get();
        }
    }

    public void join() throws ExceptionLogThread {
        if (th == null) {
            return;
        }
        try {
            th.join();
        } catch (InterruptedException e) {
            throw new ExceptionLogThread("cannot join");
        }
    }
}
