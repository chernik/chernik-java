package ru.nsu.fit.cherniak.lab2.Server.parse;

import ru.nsu.fit.cherniak.lab2.Server.stuff.MultipartContainer;
import ru.nsu.fit.cherniak.lab2.Server.exc.ExceptionMultipartParser;
import ru.nsu.fit.cherniak.lab2.Stuff;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class MultipartParser {
    private byte[] binData;
    private String drvData;
    private Map<String, MultipartContainer> parts;

    public MultipartParser(byte[] data, String delim) throws ExceptionMultipartParser {
        binData = data;
        try {
            drvData = new String(data, Stuff.byteCharset);
        } catch (UnsupportedEncodingException e) {
            throw new ExceptionMultipartParser("cannot convert to string");
        }
        delim = "--" + delim;
        String enter = "\r\n";
        String[] parts = drvData.split(delim);
        this.parts = new HashMap<>();
        int iLen = 0;
        for (int i = 0; i < parts.length; i++) {
            if (i > 0) {
                String[] pars = parts[i].split(enter);
                int jLen = 0;
                Map<String, String> map = new HashMap<>();
                String mime = "";
                for (int j = 0; j < pars.length; j++) {
                    if (j > 0) {
                        if (pars[j].length() == 0) {
                            break;
                        }
                        String[] oneParse = pars[j].split(": ");
                        switch (oneParse[0]) {
                            case "Content-Disposition":
                                String[] twoParse = oneParse[1].split("; ");
                                if (!twoParse[0].equals("form-data")) {
                                    throw new ExceptionMultipartParser("'form-data' not found");
                                }
                                String[] threeParse;
                                int kLen = 0;
                                for (int k = 1; k < twoParse.length; k++) {
                                    threeParse = twoParse[k].split("=");
                                    if (threeParse.length != 2) {
                                        throw new ExceptionMultipartParser("wrong parameter structure");
                                    }
                                    threeParse[1] = new String(
                                            Arrays.copyOfRange(
                                                    binData,
                                                    iLen +
                                                            oneParse[0].length() + 2 +
                                                            twoParse[0].length() + 2 +
                                                            kLen + 2 +
                                                            threeParse[0].length() + 1,
                                                    iLen +
                                                            oneParse[0].length() + 2 +
                                                            twoParse[0].length() + 2 +
                                                            kLen + 2 +
                                                            twoParse[k].length()
                                            )
                                    );
                                    map.put(threeParse[0], threeParse[1]);
                                    kLen += twoParse[k].length() + 2;
                                }
                                break;
                            case "Content-Type":
                                mime = oneParse[1];
                                break;
                            default:
                                oneParse[1] = new String(
                                        Arrays.copyOfRange(
                                                binData,
                                                iLen +
                                                        oneParse[0].length() + 2,
                                                iLen +
                                                        oneParse[0].length() + 2 +
                                                        oneParse[1].length()
                                        )
                                );
                                map.put(oneParse[0], oneParse[1]);
                                break;
                        }
                    }
                    jLen += pars[j].length() + enter.length();
                }
                if (!map.containsKey("name")) {
                    continue;
                }
                byte[] body = Arrays.copyOfRange(
                        binData,
                        iLen + jLen + enter.length(),
                        iLen + parts[i].length() - enter.length()
                );
                this.parts.put(
                        Stuff.delQuotes(map.get("name")),
                        new MultipartContainer(
                                map, mime, body
                        )
                );
            }
            iLen += parts[i].length() + delim.length();
        }
    }

    public Map<String, MultipartContainer> getMap() {
        return parts;
    }

    public MultipartContainer getPart(String name) {
        if (!parts.containsKey(name)) {
            return null;
        }
        return parts.get(name);
    }
}
