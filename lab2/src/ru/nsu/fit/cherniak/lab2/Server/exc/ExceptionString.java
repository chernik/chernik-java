package ru.nsu.fit.cherniak.lab2.Server.exc;

/**
 * Ошибка с пояснением
 * Может, такое уже есть, но мне лень искать)
 */
class ExceptionString extends Exception {
    private String desc;

    ExceptionString(String desc) {
        this.desc = desc;
    }

    public String get() {
        return this.desc;
    }
}
