package ru.nsu.fit.cherniak.lab2;

import ru.nsu.fit.cherniak.lab2.Server.FileServer;
import ru.nsu.fit.cherniak.lab2.Server.stuff.HttpSender;
import ru.nsu.fit.cherniak.lab2.Server.stuff.SocketEvent;
import ru.nsu.fit.cherniak.lab2.Server.exc.ExceptionHttpParser;
import ru.nsu.fit.cherniak.lab2.Server.exc.ExceptionMultipartParser;
import ru.nsu.fit.cherniak.lab2.Server.parse.HttpParser;
import ru.nsu.fit.cherniak.lab2.Server.parse.LinkParser;
import ru.nsu.fit.cherniak.lab2.Server.stuff.MultipartContainer;
import ru.nsu.fit.cherniak.lab2.Server.parse.MultipartParser;

import java.io.*;
import java.util.Map;

/**
 * Обработчик пришедшего сокета
 */
public class MySocketEvent implements SocketEvent {
    private HttpParser httpParser;
    private HttpSender httpSender;
    private LinkParser getParser;

    /**
     * Обработка пришедшего пакета
     *
     * @param httpParser данные клиента
     * @param httpSender данные для отправки
     */
    @Override
    public void onSocket(HttpParser httpParser, HttpSender httpSender) {
        this.httpParser = httpParser;
        this.httpSender = httpSender;
        String method;
        String link;
        try {
            method = httpParser.getType();
            link = httpParser.getLink();
            //httpParser.getValue(null); // Для просмотра заголовка
        } catch (ExceptionHttpParser exceptionHttpParser) {
            httpSender.sendError("parser error > " + exceptionHttpParser.get(), 405);
            return;
        }
        //traceMap(httpParser.getMap(), "HEADER"); // Для просмотра заголовка
        getParser = new LinkParser(link);
        switch (method) {
            case "POST":
                postEvent();
                break;
            case "GET":
                getEvent();
                break;
            default:
                httpSender.sendError("unknown method " + method, 405);
                break;
        }
    }

    /**
     * Обработка GET запросов
     */
    private void getEvent() {
        String echo = getParser.getValue("echo");
        if (echo != null) {
            httpSender.sendData(echo);
            return;
        }
        FileServer fileServer = new FileServer();
        File f = fileServer.getFile(getParser.getPath());
        if (f == null) {
            httpSender.sendError("not found <i>" + getParser.getPath() + "</i>", 404);
            return;
        }
        byte[] dataFile = fileServer.loadBinary(f);
        if (dataFile == null) {
            httpSender.sendError("cannot read file <i>" + getParser.getPath() + "</i>", 404);
            return;
        }
        String mime = fileServer.getMimeType(f);
        if (getParser.getValue("astext") != null) {
            mime = "text/plain";
        }
        httpSender.sendData(dataFile, mime);
    }

    /**
     * Обработка POST запросов
     */
    private void postEvent() {
        if (getParser.getValue("ispost") == null) { // Заглушка для ТЗ
            httpSender.sendError(
                    "wrong method: expect GET, find POST<br>use ?ispost to allow POST method",
                    405
            );
            return;
        }
        String fullPostType;
        try {
            fullPostType = httpParser.getValue("Content-Type");
        } catch (ExceptionHttpParser exceptionHttpParser) {
            httpSender.sendError("cannot get <i>Content-Type</i><br>" + exceptionHttpParser.get(), 405);
            return;
        }
        String postType = fullPostType.split("; ")[0];
        switch (postType) {
            case "":
                httpSender.sendError("empty <i>Content-Type</i>", 405);
                break;
            case "multipart/form-data":
                postMultipartEvent(fullPostType);
                break;
            case "application/x-www-form-urlencoded":
                postLikeGetEvent();
                break;
            default:
                httpSender.sendError("unknown type <i>" + postType + "</i>", 405);
                break;
        }
    }

    /**
     * Обработка простых POST запросов
     */
    private void postLikeGetEvent() {
        LinkParser postParser;
        try {
            postParser = new LinkParser("?" + (new String(httpParser.getBody())));
        } catch (ExceptionHttpParser exceptionHttpParser) {
            httpSender.sendError("cannot parse body<br>" + exceptionHttpParser.get(), 405);
            return;
        }
        httpSender.sendData(
                Stuff.getViewOfMap(postParser.getMap(), "<br>")
        );
    }

    /**
     * Обработка Multipart POST запросов
     *
     * @param fullPostType значение Content-Type из заголовка
     */
    private void postMultipartEvent(String fullPostType) {
        byte[] body;
        try {
            body = httpParser.getBody();
        } catch (ExceptionHttpParser exceptionHttpParser) {
            httpSender.sendError("cannot get body<br>" + exceptionHttpParser.get(), 405);
            return;
        }
        String res = getViewMultipart(
                body,
                fullPostType.split("boundary=")[1].split("; ")[0]
        );
        httpSender.sendData(res);
    }

    /**
     * Проверка на необходимость загружать файл на сервер
     * Не знаю, зачем я это вынес, видимо из-за вложенных if с большими условиями
     *
     * @param parser данные из Multipart
     * @return null в случае успеха и описание ошибки в ином случае
     */
    private String isUploadEvent(MultipartParser parser) {
        MultipartContainer mc;
        mc = parser.getPart("text");
        if (
                mc != null
                        && mc.getMimeType().length() == 0
                        && (new String(mc.getData())).equals("upload")) {
            mc = parser.getPart("file");
            if (
                    mc != null
                            && mc.getVar("filename") != null
                    ) {
                return uploadEvent(
                        Stuff.delQuotes(mc.getVar("filename")),
                        //mc.getMimeType(),
                        mc.getData()
                );
            }
        }
        return null;
    }

    /**
     * Загрузка файла
     *
     * @param name имя файла
     * @param data данные из файла
     * @return null в случае успеха, описание ошибки в случае ошибки
     */
    private String uploadEvent(String name, byte[] data) {
        File f = new File(name);
        if (!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException e) {
                return "cannot create file";
            }
            FileOutputStream fs;
            try {
                fs = new FileOutputStream(f);
            } catch (FileNotFoundException e) {
                return "cannot open file";
            }
            try {
                fs.write(data);
            } catch (IOException e) {
                return "cannot write data";
            }
            try {
                fs.close();
            } catch (IOException e) {
                return "cannot close file";
            }
        }
        return null;
    }

    /**
     * Формирование ответа на принятый Multipart
     *
     * @param data  данные из POST
     * @param delim разделитель Multipart
     * @return ответ
     */
    private String getViewMultipart(byte[] data, String delim) {
        MultipartParser parser;
        try {
            parser = new MultipartParser(data, delim);
        } catch (ExceptionMultipartParser exceptionMultipartParser) {
            return "cannot parse multipart<br>" + exceptionMultipartParser.get();
        }
        String res = Stuff.getViewOfMultipart(parser.getMap());
        String err = isUploadEvent(parser);
        if (err != null) {
            return "UPLOAD ERROR<br>" + err;
        }
        return res;
    }

    /**
     * Вывод таблицы в консоль
     * Используется для отладки
     *
     * @param map балица
     * @param head название
     */
    private void traceMap(Map<String, String> map, String head) {
        System.out.println(head + " >");
        System.out.print(Stuff.getViewOfMap(map, "\n"));
        System.out.println("---------------\n");
    }
}
