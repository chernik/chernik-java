package ru.nsu.fit.cherniak.lab4.ThreadPool;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class ThreadPool {
    private Thread ths[];
    private final List<Task> tasks;
    private boolean isStopping;
    private final Syncro async;

    public ThreadPool(int count) {
        async = new Syncro();
        ths = new Thread[count];
        tasks = new ArrayList<>();
        isStopping = false;
        for (int i = 0; i < count; i++) {
            ths[i] = new Thread(this::oneThread);
            ths[i].start();
        }
    }

    private void oneThread() {
        Task t;
        boolean isStopping = false;
        while (true) {
            synchronized (async) {
                if (this.isStopping) {
                    async.notifyS();
                    break;
                }
            }
            async.waitS(); // Blocked
            synchronized (tasks){
                if (tasks.isEmpty()) {
                    continue;
                }
                t = tasks.remove(0);
            }
            t.run();
        }
    }

    public Promise addTask(Callable<Object> cb) {
        Task t = new Task(cb);
        synchronized (tasks) {
            tasks.add(t);
        }
        async.notifyS();
        return t.getPromise();
    }

    public void stop(){
        synchronized (async){
            isStopping = true;
            async.notifyS();
        }
    }

    public void join() {
        stop();
        for (Thread th : ths) {
            try {
                th.join();
            } catch (InterruptedException ignored) {
            }
        }
    }
}
