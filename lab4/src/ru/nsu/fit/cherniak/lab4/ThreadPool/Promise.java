package ru.nsu.fit.cherniak.lab4.ThreadPool;

import java.util.concurrent.Callable;
import java.util.function.Consumer;

/**
 * async class Promise
 */
public class Promise {
    private final Callable<Object> cb; // Function
    private boolean isErrorF; // Flags
    private boolean isReadyF; // Flags
    private Exception e; // For error
    private Object result; // For data
    private final Object async; // For async thread

    /**
     * Constructor of Promise
     * @param cb Function
     */
    Promise(Callable<Object> cb){
        this.cb = cb;
        this.isErrorF = false;
        this.isReadyF = false;
        this.async = new Object();
    }

    /**
     * get function
     * @return function
     */
    public Callable<Object> getCallable(){
        return cb;
    }

    /**
     * set error result
     * @param e error
     */
    void setError(Exception e){
        synchronized (async) {
            this.e = e;
            this.result = null;
            this.isErrorF = true;
            this.isReadyF = true;
            async.notify();
        }
    }

    /**
     * set sucs result
     * @param result result
     */
    void setSucs(Object result){
        synchronized (async) {
            this.e = null;
            this.result = result;
            this.isErrorF = false;
            this.isReadyF = true;
            async.notify();
        }
    }

    /**
     * get state of thread
     * true - done
     * @return state
     */
    public boolean isReady(){
        boolean o;
        synchronized (async) {
            o = isReadyF;
        }
        return o;
    }

    /**
     * true, if we have error
     * wait for stopping thread!
     * @return boolean
     */
    public boolean isError(){
        this.goWait();
        return isErrorF;
    }

    /**
     * get error, if thread has error, else null
     * wait for stopping thread!
     * @return error
     */
    public Exception getError(){
        this.goWait();
        return this.e;
    }

    /**
     * get result of thread, if thread hasn't error, else null
     * wait for stopping thread!
     * @return result
     */
    public Object getResult(){
        this.goWait();
        return this.result;
    }

    /**
     * waiting for stopping thread
     */
    public void goWait(){
        if(isReady()){
            return;
        }
        while(true){
            try {
                this.async.wait();
            } catch (InterruptedException e1) {
                continue;
            } catch(IllegalMonitorStateException el){
                break;
            }
            break;
        }
    }

    /**
     * Running after stopping thread, if sucs
     * wait for stopping thread!
     * @param r function
     * @return this Promise
     */
    public Promise then(Consumer<Object> r){
        if(!isError()){
            r.accept(getResult());
        }
        return this;
    }

    /**
     * Running after stopping thread, if error
     * wait for stopping thread!
     * @param r
     * @return
     */
    public Promise error(Consumer<Exception> r){
        if(isError()){
            r.accept(getError());
        }
        return this;
    }
}
