package ru.nsu.fit.cherniak.lab4.GUI;

import ru.nsu.fit.cherniak.lab4.MatherServer.MatherHttpServer;
import ru.nsu.fit.cherniak.lab4.MatherServer.Mathers.AnyMather;
import ru.nsu.fit.cherniak.lab4.MatherServer.Mathers.IntMather;
import ru.nsu.fit.cherniak.lab4.MatherServer.Mathers.PathMather;
import ru.nsu.fit.cherniak.lab4.MatherServer.Mathers.StringMather;
import ru.nsu.fit.cherniak.lab4.MySocketEvent;
import ru.nsu.fit.cherniak.lab4.Server.HttpServer;
import ru.nsu.fit.cherniak.lab4.Server.exc.ExceptionHttpServer;
import ru.nsu.fit.cherniak.lab4.Server.parse.HttpParser;
import ru.nsu.fit.cherniak.lab4.Server.stuff.HttpSender;
import ru.nsu.fit.cherniak.lab4.Server.stuff.SocketEvent;
import ru.nsu.fit.cherniak.lab4.Stuff;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.Callable;

/**
 * Класс для гуишного окна
 */
public class GUIFrame extends JFrame {
    private JLabel log;
    private JButton butStart, butStop;

    private MatherHttpServer server;

    private void outMap(HttpParser httpParser, HttpSender httpSender) {
        httpSender.sendData(
                Stuff.getViewOfMap(httpParser.getPathInfo(), "<br>")
        );
    }

    /**
     * Конструктор окна и сервера
     */
    public GUIFrame() {
        server = new MatherHttpServer(4);
        server.register(Arrays.asList(
                new StringMather("name", null),
                new AnyMather("name")
        ), () -> this::outMap);
        server.register(Arrays.asList(
                new StringMather("name", null),
                new AnyMather("name"),
                new StringMather("height", "parameter"),
                new IntMather("height")
        ), () -> this::outMap);
        server.register(Arrays.asList(
                new StringMather("name", null),
                new AnyMather("name"),
                new StringMather("surname", "parameter"),
                new AnyMather("surname")
        ), () -> this::outMap);
        server.register(Collections.singletonList(
                new PathMather("path", -1)
        ), MySocketEvent::new);
        start();
    }

    /**
     * Установка элементов окна
     */
    private void start() {
        //Dimension size = new Dimension(100, 30);
        // Окно
        setDefaultLookAndFeelDecorated(true);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        setPreferredSize(new Dimension(300, 500));
        setAlwaysOnTop(true);
        setVisible(true);
        pack();
        addWindowListener(new CloseFrameEvent(this, server));
        setLocationRelativeTo(null);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        screenSize.width -= getWidth();
        screenSize.height -= getHeight() + 50;
        setLocation(screenSize.width, screenSize.height);
        // Панель логгинга
        log = new JLabel();
        log.setText("<empty>");
        //log.setPreferredSize(size);
        //log.setHorizontalAlignment(JLabel.LEFT);
        //log.setVerticalAlignment(JLabel.TOP);
        add(log, BorderLayout.NORTH);
        // Таймер для логгинга
        Timer tm = new Timer(100, e -> tmEvent());
        // Кнопка старта сервера
        butStart = new JButton();
        butStart.setText("Start");
        //butStart.setPreferredSize(size);
        //butStart.setHorizontalAlignment(JLabel.LEFT);
        //butStart.setVerticalAlignment(JLabel.TOP);
        //butStart.setLocation(0,50);
        butStart.addMouseListener((EventClick) this::butStartEvent);
        add(butStart, BorderLayout.CENTER);
        // Кнопка остановки сервера
        butStop = new JButton();
        butStop.setText("Stop");
        //butStop.setPreferredSize(size);
        //butStop.setHorizontalAlignment(JLabel.LEFT);
        //butStop.setVerticalAlignment(JLabel.TOP);
        butStop.addMouseListener((EventClick) this::butStopEvent);
        add(butStop, BorderLayout.SOUTH);
        // Запуск элементов
        tm.start();
        //butStartEvent();
    }

    /**
     * Обработчик таймера
     * Выполняет лог сервера
     */
    private void tmEvent() {
        String state = server.getState();
        if (state.equals("")) {
            state = "<empty>";
        }
        log.setText(state);
        if (server.isEnded()) {
            butStop.setEnabled(false);
            butStart.setEnabled(true);
        } else {
            butStop.setEnabled(true);
            butStart.setEnabled(false);
        }
    }

    /**
     * Обработчик кнопки запуска сервера
     */
    private void butStartEvent() {
        if (!butStart.isEnabled()) {
            return;
        }
        try {
            server.start();
        } catch (ExceptionHttpServer exceptionHttpServer) {
            log.setText("ERROR > at start");
        }
    }

    /**
     * Обработчик кнопки остановки сервера
     */
    private void butStopEvent() {
        if (!butStop.isEnabled()) {
            return;
        }
        try {
            server.stop();
        } catch (ExceptionHttpServer exceptionHttpServer) {
            log.setText("ERROR > stop");
        }
    }
}
