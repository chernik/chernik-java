package ru.nsu.fit.cherniak.lab4.GUI;

import ru.nsu.fit.cherniak.lab4.MatherServer.MatherHttpServer;
import ru.nsu.fit.cherniak.lab4.Server.HttpServer;
import ru.nsu.fit.cherniak.lab4.Server.exc.ExceptionHttpServer;
import ru.nsu.fit.cherniak.lab4.Server.exc.ExceptionLogThread;

import javax.swing.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 * Обработчик закрытия окна
 */
class CloseFrameEvent implements WindowListener {
    private JFrame f;
    private HttpServer server;

    CloseFrameEvent(JFrame f, HttpServer server) {
        this.f = f;
        this.server = server;
    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    /**
     * Собсно функция, вызываемая по закрытии
     *
     * @param e событие
     */
    @Override
    public void windowClosing(WindowEvent e) {
        if (server.isEnded()) {
            f.dispose();
            try {
                server.stop();
            } catch (ExceptionHttpServer ignored) {}
            f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        } else {
            JOptionPane.showMessageDialog(f,
                    "Firstly stop the server!",
                    "ERROR",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
