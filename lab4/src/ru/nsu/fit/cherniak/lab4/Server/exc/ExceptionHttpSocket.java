package ru.nsu.fit.cherniak.lab4.Server.exc;

/**
 * Ошибка класса HttpSocket
 */
public class ExceptionHttpSocket extends ExceptionString {
    public ExceptionHttpSocket(String desc) {
        super(desc);
    }
}
