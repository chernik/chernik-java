package ru.nsu.fit.cherniak.lab4.Server.parse;

import ru.nsu.fit.cherniak.lab4.Server.exc.ExceptionHttpParser;
import ru.nsu.fit.cherniak.lab4.Stuff;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Парсер входных данных в сокете
 * Загружает данные только по запросу
 */
public class HttpParser {
    private InputStream is;
    private Map<String, String> head = new HashMap<>();
    private BufferedInputStream isDrv;
    private boolean noMore; // Весь заголовок считан
    private byte[] body;
    private Map<String, String> pathInfo; // Данные для 4 лабы, вклинено по-жесткому, ну лан

    /**
     * Конструктор
     *
     * @param is InputStream из сокета
     */
    public HttpParser(InputStream is) {
        this.is = is;
        this.isDrv = null;
        this.noMore = false;
        this.body = null;
        this.pathInfo = new HashMap<>();
    }

    /**
     * Взять тип запроса (GET или другой)
     *
     * @return Тип запроса
     * @throws ExceptionHttpParser ошибка
     */
    public String getType() throws ExceptionHttpParser {
        parseFirst();
        return head.get("type");
    }

    /**
     * Взять переданную ссылку
     *
     * @return Ссылка
     * @throws ExceptionHttpParser ошибка
     */
    public String getLink() throws ExceptionHttpParser {
        parseFirst();
        return head.get("link");
    }

    /**
     * Взять тип HTTP протокола
     * Пустая строка в случае отсутствия подобного
     *
     * @return Тип HTTP
     * @throws ExceptionHttpParser ошибка
     */
    public String getVersion() throws ExceptionHttpParser {
        parseFirst();
        return head.get("version");
    }

    /**
     * Взять значение по имени поля
     * Пустая строка вернется в случае отсутствия подобного поля
     *
     * @param key Имя поля
     * @return Значение поля
     * @throws ExceptionHttpParser ошибка
     */
    public String getValue(String key) throws ExceptionHttpParser {
        if (!head.containsKey(key)) {
            parseFirst(); // Для подгрузки в буффер первой строки
            if (!head.containsKey(key) && !parseValue(key)) {
                return "";
            }
        }
        return head.get(key);
    }

    /**
     * Возвращает тело запроса
     * null, если данные не получены
     *
     * @return BufferReader, содержащий тело
     * @throws ExceptionHttpParser ошибка
     */
    public byte[] getBody() throws ExceptionHttpParser {
        if (body != null) {
            return body;
        }
        getType();
        parseValue(null);
        String keyLen = "Content-Length";
        int len;
        if (head.containsKey(keyLen)) {
            len = Integer.valueOf(head.get(keyLen));
        } else {
            return null;
        }
        if (len <= 0) {
            return null;
        }
        body = new byte[len];
        int lenReal = 0;
        try {
            while (lenReal < len) {
                lenReal += isDrv.read(body, lenReal, len - lenReal);
            }
        } catch (IOException e) {
            throw new ExceptionHttpParser("read body stream error");
        }
        if (lenReal != len) {
            throw new ExceptionHttpParser("read body length error<br>" + len + ", but " + lenReal);
        }
        return body;
    }

    /**
     * Парсим первую строку
     *
     * @throws ExceptionHttpParser ошибка
     */
    private void parseFirst() throws ExceptionHttpParser {
        if (head.containsKey("type") || noMore) {
            return;
        }
        String str = getString();
        if (str == null) {
            return;
        }
        String[] strParse = str.split(" ");
        if (strParse.length < 2 || strParse.length > 3) {
            throw new ExceptionHttpParser("unknown type");
        }
        head.put("type", strParse[0]);
        head.put("link", strParse[1]);
        if (strParse.length == 3) {
            head.put("version", strParse[2]);
        } else {
            head.put("version", "");
        }
    }

    /**
     * Парсим параметры в заголовке до пустой строки
     *
     * @param value Нужный ключ. null для загрузки всех оставшихся ключей
     * @return Возвращает true, если ключ найден, и false в противном случае
     * @throws ExceptionHttpParser ошибка
     */
    private boolean parseValue(String value) throws ExceptionHttpParser {
        if (noMore) {
            return false;
        }
        String str;
        String[] strParse;
        while (true) {
            str = getString();
            if (str == null || str.length() == 0) {
                noMore = true;
                return false;
            }
            strParse = str.split(": ");
            if (strParse.length != 2) {
                throw new ExceptionHttpParser("unknown type");
            }
            if (!head.containsKey(strParse[0])) {
                head.put(strParse[0], strParse[1]);
            }
            if (value != null && strParse[0].equals(value)) {
                return true;
            }
        }
    }

    /**
     * Считываем очередную строку
     */
    private String getString() throws ExceptionHttpParser {
        String str;
        buffering();
        try {
            str = new String(Stuff.getLineRNFromStream(isDrv), Stuff.byteCharset);
        } catch (IOException e) {
            throw new ExceptionHttpParser("no input data");
        }
        return str;
    }

    /**
     * Буферизируем
     */
    private void buffering() {
        if (isDrv == null) {
            isDrv = new BufferedInputStream(is);
        }
    }

    /**
     * Возвращаем таблицу параметров заголовка
     *
     * @return Таблица
     */
    public Map<String, String> getMap() {
        return head;
    }

    /**
     * берем pathInfo
     *
     * @return pathInfo
     */
    public Map<String, String> getPathInfo() {
        return pathInfo;
    }

    /**
     * сохраняем новый ненулевой pathInfo
     *
     * @param pathInfo pathInfo
     */
    public void setPathInfo(Map<String, String> pathInfo) {
        if (pathInfo == null) {
            throw new NullPointerException();
        }
        this.pathInfo = pathInfo;
    }
}
