package ru.nsu.fit.cherniak.lab4.Server.stuff;

import java.util.Map;

public class MultipartContainer {

    private Map<String, String> vars;
    private String mime;
    private byte[] data;

    public Map<String, String> getMap() {
        return vars;
    }

    public String getMimeType() {
        return mime;
    }

    public byte[] getData() {
        return data;
    }

    public String getVar(String key) {
        if (!vars.containsKey(key)) {
            return null;
        }
        return vars.get(key);
    }

    public MultipartContainer(Map<String, String> vars, String mime, byte[] data) {

        this.vars = vars;
        this.mime = mime;
        this.data = data;
    }
}
