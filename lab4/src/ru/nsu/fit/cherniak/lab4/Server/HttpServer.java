package ru.nsu.fit.cherniak.lab4.Server;

import ru.nsu.fit.cherniak.lab4.Server.stuff.LogThread;
import ru.nsu.fit.cherniak.lab4.Server.stuff.SocketEvent;
import ru.nsu.fit.cherniak.lab4.Server.exc.ExceptionHttpServer;
import ru.nsu.fit.cherniak.lab4.Server.exc.ExceptionHttpSocket;
import ru.nsu.fit.cherniak.lab4.ThreadPool.ThreadPool;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.concurrent.Callable;

/**
 * Сервер в отдельном потоке с логгированием
 */
public class HttpServer extends LogThread {
    private int port; // Порт
    private boolean isStopping; // Для остановки потока
    private HttpSocket hs; // Сокет
    private Callable<SocketEvent> event; // Обработчик сокета
    private ThreadPool thp; // Пул для обработчиков сокетов
    int sizePool; // Размер пула

    /**
     * Установка нового порта
     *
     * @param port порт
     */
    public void setPort(int port) {
        this.port = port;
    }

    /**
     * Конструктор сервера
     *
     * @param sizePool размер ThreadPool
     */
    public HttpServer(int sizePool) {
        thp = null;
        this.sizePool = sizePool;
        port = 8080;
        isStopping = false;
        hs = null;
    }

    /**
     * Установка обработчика сокетов
     *
     * @param event Конструктор обработчика
     * @throws ExceptionHttpServer В данном классе исключение не выкидывается
     */
    public void setEvent(Callable<SocketEvent> event) throws ExceptionHttpServer {
        this.event = event;
    }

    /**
     * Запуск сервера
     * Невозможен при уже запущенном сервере
     */
    public void start() throws ExceptionHttpServer {
        if(thp == null) {
            thp = new ThreadPool(sizePool + 1); // Учитываем, что наш сервер тоже должен где-то выполняться
        }
        if (!super.isEnded()) {
            throw new ExceptionHttpServer("Server already run");
        }
        if (this.event == null) {
            throw new ExceptionHttpServer("Firstly set event creator");
        }
        isStopping = false;
        setState("starting...");
        thp.addTask(super.getThread(this::startServer)); // Запускаем сервер
    }

    /**
     * Остановка сервера
     * Невозможна при уже остановленном сервере
     * Сервер может сразу не остановиться, необходимо следить либо за состоянием (must be 'stopped') либо isStopped()
     */
    public void stop() throws ExceptionHttpServer {
        if (super.isEnded() || isStopping) {
            throw new ExceptionHttpServer("Already stopped or stopping");
        }
        isStopping = true;
        setState("stopping...");
    }

    /**
     * Полная остановка с ожиданием
     * @throws ExceptionHttpServer ы
     */
    public void join() throws ExceptionHttpServer {
        stop();
        thp.join();
        thp = null;
    }

    public void restart() throws ExceptionHttpServer {
        (new Thread(()->{
            try {
                this.join();
                this.start();
            } catch (ExceptionHttpServer exceptionHttpServer) {
                exceptionHttpServer.printStackTrace();
            }
        })).start();
    }

    /**
     * Поток сервера
     */
    private void startServer() {
        ServerSocket ss;
        try {
            ss = new ServerSocket(port);
        } catch (IOException e) {
            setError("no open port");
            return;
        }
        try {
            ss.setSoTimeout(1000);
        } catch (SocketException e) {
            setError("no set timeout");
            return;
        }
        Socket s = null;
        while (true) {
            setState("waiting...");
            try {
                s = ss.accept();
            } catch (SocketTimeoutException e) {
                if (isStopping) {
                    break;
                } else {
                    continue;
                }
            } catch (IOException e) {
                setError("cannot get socket");
                continue;
            }
            if (isStopping) {
                break;
            }
            setState("catched");
            startSocket(s);
        }
        try {
            ss.close();
        } catch (IOException e) {
            setError("cannot close server");
        }
        setState("stopped");
    }

    /**
     * Обработка запроса
     */
    private void startSocket(Socket s) {
        try {
            hs = new HttpSocket(s, event.call());
        } catch (ExceptionHttpSocket exceptionHttpSocket) {
            setError("socket error > " + exceptionHttpSocket.get());
            return;
        } catch (Exception e) {
            setError("cannot create socket event");
        }
        setState("doing socket...");
        thp.addTask(hs.getThread());
    }

    /**
     * Взять текущее состояние
     * Если есть сокет, то добавить состояние сокета
     *
     * @return состояние
     */
    @Override
    public String getState() {
        if (hs == null) {
            return super.getState();
        }
        return super.getState() + " > " + hs.getState();
    }
}
