package ru.nsu.fit.cherniak.lab4.Server.stuff;

import ru.nsu.fit.cherniak.lab4.Server.parse.HttpParser;

public interface SocketEvent {
    void onSocket(HttpParser httpParser, HttpSender httpSender);
}
