package ru.nsu.fit.cherniak.lab4.Server.exc;

/**
 * Ошибка класса HttpParser
 */
public class ExceptionHttpParser extends ExceptionString {
    public ExceptionHttpParser(String desc) {
        super(desc);
    }
}
