package ru.nsu.fit.cherniak.lab4;

import ru.nsu.fit.cherniak.lab4.GUI.GUIFrame;

public class Main {
    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(GUIFrame::new);
    }
}
