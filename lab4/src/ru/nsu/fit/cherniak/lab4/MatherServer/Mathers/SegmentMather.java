package ru.nsu.fit.cherniak.lab4.MatherServer.Mathers;

import java.util.List;
import java.util.Map;

public interface SegmentMather {

    int math(List<String> path, Map<String, String> map, int ind);

    @Override
    int hashCode();

    @Override
    boolean equals(Object other);
}
