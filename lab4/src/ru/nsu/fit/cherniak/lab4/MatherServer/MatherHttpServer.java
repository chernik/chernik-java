package ru.nsu.fit.cherniak.lab4.MatherServer;

import ru.nsu.fit.cherniak.lab4.MatherServer.Mathers.SegmentMather;
import ru.nsu.fit.cherniak.lab4.Server.HttpServer;
import ru.nsu.fit.cherniak.lab4.Server.exc.ExceptionHttpServer;
import ru.nsu.fit.cherniak.lab4.Server.stuff.SocketEvent;

import java.util.List;
import java.util.concurrent.Callable;

public class MatherHttpServer extends HttpServer {
    private MathersTree tree = new MathersTree();

    /**
     * Using our socket event
     *
     * @param sizePool size of ThreadPool
     */
    public MatherHttpServer(int sizePool) {
        super(sizePool);
        try {
            super.setEvent(() -> new MainMatherEvent(tree));
        } catch (ExceptionHttpServer ignored) {
        }
    }

    /**
     * Registrate
     *
     * @param mathers
     * @param event
     */
    public void register(List<SegmentMather> mathers, Callable<SocketEvent> event) {
        if (!tree.register(mathers, event)) {
            throw new ExceptionInInitializerError("WOW");
        }
    }

    /**
     * Override for block replaseing socket event of our http server
     *
     * @param event Constructor of event
     * @throws ExceptionHttpServer Permanent error on excecute that method
     */
    @Override
    public void setEvent(Callable<SocketEvent> event) throws ExceptionHttpServer {
        throw new ExceptionHttpServer("Cannot set event in MatherHttpServer");
    }
}
