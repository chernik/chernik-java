package ru.nsu.fit.cherniak.lab4.MatherServer;

import ru.nsu.fit.cherniak.lab4.MatherServer.Mathers.SegmentMather;
import ru.nsu.fit.cherniak.lab4.Server.stuff.SocketEvent;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

class MathersTree {
    private List<MathersTree> next;
    private SegmentMather mather;
    private Callable<SocketEvent> event;

    MathersTree() {
        next = new LinkedList<>();
        mather = null;
        event = null;
    }

    /**
     * Find event in tree
     *
     * @param path path
     * @param map  filling map
     * @return constructor of event
     */
    public Callable<SocketEvent> math(List<String> path, Map<String, String> map) {
        return math(path, map, 0);
    }

    private Callable<SocketEvent> math(List<String> path, Map<String, String> map, int ind) {
        if (mather != null) {
            int pos = mather.math(path, map, ind);
            if (pos < 0) {
                return null;
            }
            ind += pos + 1;
        }
        if (ind == path.size()) {
            return this.event;
        }
        Callable<SocketEvent> event;
        Map<String,String> bufMap = new HashMap<>();
        for (MathersTree next : this.next) {
            bufMap.clear();
            event = next.math(path, bufMap, ind);
            if (event != null) {
                bufMap.forEach(map::put);
                return event;
            }
        }
        return this.event;
    }

    /**
     * Add list of mathers in tree
     *
     * @param mathers mathers
     * @param event   creator of event
     * @return true - sucs, false - never return, maybe)
     */
    public boolean register(List<SegmentMather> mathers, Callable<SocketEvent> event) {
        return register(mathers, event, -1);
    }

    private boolean register(List<SegmentMather> mathers, Callable<SocketEvent> event, int ind) {
        if (ind >= 0 && mather != null) {
            if (!mather.equals(mathers.get(ind))) {
                return false;
            }
        }
        if (ind >= 0 && mather == null) {
            mather = mathers.get(ind);
        }
        ind++;
        if (ind == mathers.size()) {
            this.event = event;
            return true;
        }
        for (MathersTree next : this.next) {
            if (next.register(mathers, event, ind)) {
                return true;
            }
        }
        MathersTree mathersTree = new MathersTree();
        this.next.add(mathersTree);
        return mathersTree.register(mathers, event, ind);
    }
}
