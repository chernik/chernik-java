package ru.nsu.fit.cherniak.lab4.MatherServer.Mathers;

import java.util.List;
import java.util.Map;

public class StringMather implements SegmentMather {
    private String str;
    private String field;

    public StringMather(String str, String field) {
        this.str = str;
        this.field = field;
    }

    @Override
    public int hashCode() {
        return str.hashCode() + (field == null ? 0 : field.hashCode());
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof StringMather) {
            if (other.hashCode() != this.hashCode()) {
                return false;
            }
            if (!this.str.equals(((StringMather) other).str)) {
                return false;
            }
            if (this.field == null) {
                return ((StringMather) other).field == null;
            }
            return this.field.equals(((StringMather) other).field);
        }
        return false;
    }

    @Override
    public int math(List<String> path, Map<String, String> map, int ind) {
        if (path.get(ind).equals(str)) {
            if (field != null) {
                map.put(field, str);
            }
            return 0;
        }
        return -1;
    }
}
