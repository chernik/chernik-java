package ru.nsu.fit.cherniak.lab4.MatherServer;

import ru.nsu.fit.cherniak.lab4.Server.exc.ExceptionHttpParser;
import ru.nsu.fit.cherniak.lab4.Server.parse.HttpParser;
import ru.nsu.fit.cherniak.lab4.Server.parse.LinkParser;
import ru.nsu.fit.cherniak.lab4.Server.stuff.HttpSender;
import ru.nsu.fit.cherniak.lab4.Server.stuff.SocketEvent;

import java.util.*;
import java.util.concurrent.Callable;

/**
 * Event for MatherHttpServer
 */
class MainMatherEvent implements SocketEvent {
    private MathersTree tree;

    MainMatherEvent(MathersTree tree) {
        this.tree = tree;
    }

    @Override
    public void onSocket(HttpParser httpParser, HttpSender httpSender) {
        String link;
        try {
            link = httpParser.getLink();
        } catch (ExceptionHttpParser exceptionHttpParser) {
            httpSender.sendError("Cannot get link", 405);
            return;
        }
        LinkParser linkParser = new LinkParser(link);
        String path = linkParser.getPath();
        List<String> list = new LinkedList<>();
        list.addAll(Arrays.asList(path.split("/", -1)));
        Map<String, String> map = new HashMap<>();
        Callable<SocketEvent> res = tree.math(list, map);
        if (res == null) {
            httpSender.sendError(404);
            return;
        }
        httpParser.setPathInfo(map);
        try {
            res.call().onSocket(httpParser, httpSender);
        } catch (Exception ignored) {
        }
    }
}
