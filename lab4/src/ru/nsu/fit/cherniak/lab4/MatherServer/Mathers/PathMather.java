package ru.nsu.fit.cherniak.lab4.MatherServer.Mathers;

import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

public class PathMather implements SegmentMather {
    private String name;
    private int count;

    public PathMather(String name, int count) {
        this.name = name;
        this.count = count;
    }

    @Override
    public int hashCode() {
        return count + (name == null ? 0 : name.hashCode());
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof PathMather) {
            if (other.hashCode() != this.hashCode()) {
                return false;
            }
            if (this.count != ((PathMather) other).count) {
                return false;
            }
            if (this.name == null) {
                return ((PathMather) other).name == null;
            }
            return this.name.equals(((PathMather) other).name);
        }
        return false;
    }

    @Override
    public int math(List<String> path, Map<String, String> map, int ind) {
        int up = count < 0 ? path.size() : ind + count;
        if (name != null) {
            StringJoiner s = new StringJoiner("/");
            for (int i = ind; i < up; i++) {
                s.add(path.get(i));
            }
            map.put(name, s.toString());
        }
        return up - ind - 1;
    }
}
