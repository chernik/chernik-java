package ru.nsu.fit.cherniak.lab5;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.util.Arrays;

public class JSStuff {
    /**
     * Name of part of stack of thread
     */
    static private final String stackPartName = "jdk.scripting.nashorn.scripts";

    /**
     * Thread.sleep(millis) with no exception
     * Exception prints to console
     *
     * @param millis arg of Thread.sleep
     */
    static public void sleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            System.err.println("Interrupted sleep!");
        }
    }

    /**
     * Trace method
     * Out all parameters of part of stack
     *
     * @param stackTraceElement part of stack
     * @return String
     */
    static public String getInfo(StackTraceElement stackTraceElement) {
        return stackTraceElement.toString() + "\n" +
                "ClassName > " + stackTraceElement.getClassName() + "\n" +
                "MethodName > " + stackTraceElement.getMethodName() + "\n" +
                "ClassLoaderName > " + stackTraceElement.getClassLoaderName() + "\n" +
                "FileName > " + stackTraceElement.getFileName() + "\n" +
                "ModuleName > " + stackTraceElement.getModuleName() + "\n" +
                "ModuleVersion > " + stackTraceElement.getModuleVersion() + "\n" +
                "LineNumber > " + stackTraceElement.getLineNumber() + "\n--------------";
    }

    /**
     * Return stack trace of JS
     *
     * @param th thread
     * @return String[] stack
     */
    static public String[] getStackTrace(StackTraceElement[] th) {
        return Arrays.stream(th) // Stack of thread
                .filter(el -> stackPartName.equals(el.getModuleName())) // Filter JS stack
                .map(el -> "at " + el.getMethodName() +
                        " in line " + String.valueOf(el.getLineNumber()))
                .toArray(String[]::new);
    }

    /**
     * Create empty engine
     *
     * @param ctx Context
     * @return Engine
     */
    static public ScriptEngine getEngine(Object ctx) {
        ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");
        if (engine == null) {
            return null;
        }
        Bindings b = engine.getBindings(ScriptContext.ENGINE_SCOPE);
        b.remove("print");
        b.remove("Java");
        b.put("Context", ctx);
        return engine;
    }
}
