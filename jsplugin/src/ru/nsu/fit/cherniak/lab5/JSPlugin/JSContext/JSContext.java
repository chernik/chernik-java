package ru.nsu.fit.cherniak.lab5.JSPlugin.JSContext;

import ru.nsu.fit.cherniak.lab5.Stuff;
import ru.nsu.fit.cherniak.lab5.ThreadPool.ThreadPool;

import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Общий контекст для всех JS контекстов
 */
public class JSContext {
    private Map<String, JSOne> map;
    private ThreadPool pool;

    /**
     * Конструктор
     */
    public JSContext(int kern) {
        map = new HashMap<>();
        pool = new ThreadPool(kern);
    }

    /**
     * Выделить отдельный JS контекст
     *
     * @return хэш контекста - по нему можно обратиться к контексту
     */
    public String newItem() {
        JSOne one = new JSOne(pool);
        String key = String.valueOf(one.hashCode()) + " " + new Date().getTime();
        try {
            key = Stuff.getMD5(key);
        } catch (NoSuchAlgorithmException ignored) {
        }
        map.put(key, one);
        return key;
    }

    /**
     * Взять JS контекст по хэшу
     *
     * @param key хэш
     * @return контекст либо null, если контекст не найден
     */
    public JSOne getItem(String key) {
        return map.get(key);
    }

    /**
     * Удалить JS контекст
     *
     * @param key хэш
     * @return false, если контекст не найден
     */
    public boolean removeItem(String key) {
        if (!map.containsKey(key)) {
            return false;
        }
        map.remove(key);
        return true;
    }

    /**
     * Завершает все потоки и удаляет ThreadPool
     */
    public void destroy() {
        map.forEach((key, val) -> val.stop());
        pool.join();
    }
}
