package ru.nsu.fit.cherniak.lab5.JSPlugin;

import ru.nsu.fit.cherniak.lab5.JSPlugin.JSContext.JSContext;
import ru.nsu.fit.cherniak.lab5.JSStuff;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Main {

    private static void testJSContext() {
        byte[] file;
        System.out.println("Load from test.js");
        InputStream f;
        f = JSContext.class.getClassLoader().getResourceAsStream("test.js");
        if(f == null){
            System.out.println("Cannot find file");
            return;
        }
        try {
            file = f.readAllBytes();
            f.close();
        } catch (IOException e) {
            System.out.println("Read error");
            return;
        }
        JSContext ctx = new JSContext(1); // Use one kernel
        String hash = ctx.newItem();
        System.out.println("hash = " + hash + "\n----------------");
        System.out.println(ctx.getItem(hash).getState() + "\n---------------");
        for (int i = 0; i < 3; i++) {
            ctx.getItem(hash).start(file);
            JSStuff.sleep(1000);
            System.out.println(ctx.getItem(hash).getState() + "\n--------------");
            ctx.getItem(hash).stop();
            JSStuff.sleep(2000);
            System.out.println(ctx.getItem(hash).getState() + "\n--------------");
        }
        ctx.destroy();
        System.out.println("The end");
    }

    public static void main(String[] args) {
        testJSContext();
    }
}
