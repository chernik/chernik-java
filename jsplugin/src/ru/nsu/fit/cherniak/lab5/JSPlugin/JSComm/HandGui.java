package ru.nsu.fit.cherniak.lab5.JSPlugin.JSComm;

import ru.nsu.fit.cherniak.lab5.JSPlugin.JSContext.JSContext;
import ru.nsu.fit.cherniak.lab5.JSPlugin.JSContext.JSOne;
import ru.nsu.fit.cherniak.lab5.Server.parse.HttpParser;
import ru.nsu.fit.cherniak.lab5.Server.stuff.HttpSender;
import ru.nsu.fit.cherniak.lab5.Server.stuff.SocketEvent;

import java.io.FileInputStream;
import java.io.IOException;

/**
 * Пользовательский интерфейс
 */
public class HandGui implements SocketEvent {
    private JSContext ctx;
    private byte[] htm;

    public HandGui(JSContext ctx, byte[] htm) {
        this.ctx = ctx;
        this.htm = htm;
    }

    @Override
    public void onSocket(HttpParser httpParser, HttpSender httpSender) {
        String hash = httpParser.getPathInfo().get("hash");
        JSOne one = ctx.getItem(hash);
        httpSender.sendData(htm, "text/html");
    }
}
