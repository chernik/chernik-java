package ru.nsu.fit.cherniak.lab5.JSPlugin.JSContext;

import ru.nsu.fit.cherniak.lab5.JSStuff;
import ru.nsu.fit.cherniak.lab5.ThreadPool.ThreadPool;

import javax.script.ScriptEngine;
import javax.script.ScriptException;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Date;

/**
 * Отдельный JS контекст
 */
public class JSOne {
    private static final int SLEEP = 500; // Шаги проверки

    /**
     * Состояние выполнения
     */
    private final int NOTHING = 0; // Ничего в данный момент не выполняется
    private final int QUEUE = 1; // В очереди на выполнение
    private final int RUNNING = 2; // Выполняется
    private final int SUCCESS = 3; // Успешно завершен
    private final int ERROR = 4; // Завершен с ошибкой (ошибка в err)
    private final int INTERRUPTED = 5; // Выполнение прервано

    private final Object sync; // Для всего, кроме pool и intro
    private Thread th; // Поток выполнения
    private ThreadPool pool; // ThreadPool из общего контекста
    private ScriptEngine engine; // Движок
    private JSIntro intro; // Связь JS -> Java
    private Throwable err; // Если поток завершился с ошибкой
    private Date start; // Время запуска JS
    private Date finish; // Время завершения jS
    private int state; // Текущее состояние выполнения
    private byte[] code; // Отправленный код

    /**
     * Конструктор
     *
     * @param pool ThreadPool из общего контекста
     */
    public JSOne(ThreadPool pool) {
        this.pool = pool;
        intro = new JSIntro();
        state = NOTHING;
        sync = new Object();
        code = "// No code yet".getBytes();
        err = null;
    }

    /**
     * Запуск JS файла
     *
     * @param code JS код
     * @return true при успешном запуске
     */
    public boolean start(final byte[] code) {
        synchronized (sync) {
            if (th != null) { // Если уже работает
                return false;
            }
            state = QUEUE;
            this.code = code;
            intro.resetPrintBuilder();
            engine = JSStuff.getEngine(intro); // Новый движок
            th = new Thread(() -> inPool( // Поток выполнения
                    new InputStreamReader(new ByteArrayInputStream(code))
            ));
            start = new Date();
        }
        pool.addTask(this::forPool);
        return true;
    }

    private void inPool(final Reader code) {
        synchronized (sync) {
            state = RUNNING; // Выполняется
            err = null;
        }
        try {
            engine.eval(code);
            synchronized (sync) {
                state = SUCCESS; // Успешно завершено
            }
        } catch (ScriptException e) {
            synchronized (sync) {
                err = e;
                state = ERROR; // Завершено с ошибкой
            }
        } catch (RuntimeException e) {
            Throwable throwable = e.getCause();
            if (throwable instanceof InterruptedException) {
                synchronized (sync) {
                    state = INTERRUPTED;
                    err = throwable;
                }
                return;
            }
            System.err.println("Uncaught error");
            throwable.printStackTrace();
            synchronized (sync) {
                err = throwable;
                state = ERROR;
            }
        } catch (StackOverflowError e) {
            synchronized (sync) {
                err = e;
                state = INTERRUPTED;
            }
        }
    }

    /**
     * Код, посылаемый в ThreadPool
     *
     * @return Object
     */
    private Object forPool() {
        synchronized (sync) {
            th.start();
        }
        Thread loopTh;
        while (true) {
            JSStuff.sleep(SLEEP);
            synchronized (sync) {
                loopTh = th;
            }
            if (loopTh == null) {
                System.err.println("Unexpected null thread");
                break;
            }
            if (!loopTh.isAlive()) {
                break;
            }
            if (loopTh.isInterrupted()) {
                JSStuff.sleep(SLEEP);
                if (loopTh.isAlive()) {
                    loopTh.stop();
                    synchronized (sync) {
                        err = null;
                    }
                }
                synchronized (sync) {
                    state = INTERRUPTED;
                }
                break;
            }
        }
        synchronized (sync) {
            th = null;
            finish = new Date();
        }
        return null;
    }

    /**
     * Описание состояния JS контекста
     *
     * @return состояние
     */
    public String getState() {
        int state;
        Throwable err;
        Thread th;
        String intro;
        String time = null;
        synchronized (sync) {
            state = this.state;
            th = this.th;
            err = this.err;
            intro = this.intro.getPrintContent();
            if (state == SUCCESS) {
                time = getTime();
            }
        }
        switch (state) {
            case NOTHING:
                return "Empty";
            case RUNNING:
                return "Running\n" + intro + "\n" +
                        String.join("\n", JSStuff.getStackTrace(th.getStackTrace()));
            case ERROR:
                return "Error\n" + err.getMessage() + "\n" +
                        String.join("\n", JSStuff.getStackTrace(err.getStackTrace()));
            case INTERRUPTED:
                return "Interrupted" + ((err == null) ? "" : (
                        "\n" + err.toString() + "\n" +
                                String.join("\n", JSStuff.getStackTrace(err.getStackTrace()))
                ));
            case SUCCESS:
                return "Successfully ended\nRuntime " + time +
                        "\nOutput >\n" + intro;
            case QUEUE:
                return "In queue";
        }
        return "Unknown state";
    }

    /**
     * Остановка потока
     *
     * @return true при успехе
     */
    public boolean stop() {
        synchronized (sync) {
            if (th == null) {
                return false;
            }
            th.interrupt();
        }
        return true;
    }

    private String getTime() {
        return String.valueOf((finish.getTime() - start.getTime()) / 1000) + " sec";
    }

    public byte[] getCode() {
        return code;
    }
}
