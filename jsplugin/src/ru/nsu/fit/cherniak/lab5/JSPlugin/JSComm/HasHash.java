package ru.nsu.fit.cherniak.lab5.JSPlugin.JSComm;

import ru.nsu.fit.cherniak.lab5.JSPlugin.JSContext.JSContext;
import ru.nsu.fit.cherniak.lab5.JSPlugin.JSContext.JSOne;
import ru.nsu.fit.cherniak.lab5.MatherServer.Interceptors.Interceptor;
import ru.nsu.fit.cherniak.lab5.Server.parse.HttpParser;
import ru.nsu.fit.cherniak.lab5.Server.stuff.HttpSender;

public class HasHash implements Interceptor {
    private JSContext ctx;

    public HasHash(JSContext ctx) {
        this.ctx = ctx;
    }

    @Override
    public boolean check(HttpParser httpParser, HttpSender httpSender) {
        String hash = httpParser.getPathInfo().get("hash");
        JSOne one = ctx.getItem(hash);
        if (one == null) {
            httpSender.sendError("Invalid hash", 502);
            return true;
        }
        return false;
    }

    @Override
    public String getName() {
        return "JS Plugin has hash?";
    }
}
