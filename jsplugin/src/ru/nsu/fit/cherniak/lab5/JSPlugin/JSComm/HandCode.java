package ru.nsu.fit.cherniak.lab5.JSPlugin.JSComm;

import ru.nsu.fit.cherniak.lab5.JSPlugin.JSContext.JSContext;
import ru.nsu.fit.cherniak.lab5.JSPlugin.JSContext.JSOne;
import ru.nsu.fit.cherniak.lab5.Server.parse.HttpParser;
import ru.nsu.fit.cherniak.lab5.Server.stuff.HttpSender;
import ru.nsu.fit.cherniak.lab5.Server.stuff.SocketEvent;

/**
 * Возвращает последний запущенный код
 */
public class HandCode implements SocketEvent {
    private JSContext ctx;

    public HandCode(JSContext ctx) {
        this.ctx = ctx;
    }

    @Override
    public void onSocket(HttpParser httpParser, HttpSender httpSender) {
        String hash = httpParser.getPathInfo().get("hash");
        JSOne one = ctx.getItem(hash);
        httpSender.sendData(one.getCode(), "text/plain");
    }
}
