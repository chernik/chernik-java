package ru.nsu.fit.cherniak.lab5.JSPlugin.JSComm;

import ru.nsu.fit.cherniak.lab5.JSPlugin.JSContext.JSContext;
import ru.nsu.fit.cherniak.lab5.JSPlugin.JSContext.JSOne;
import ru.nsu.fit.cherniak.lab5.Server.exc.ExceptionHttpParser;
import ru.nsu.fit.cherniak.lab5.Server.parse.HttpParser;
import ru.nsu.fit.cherniak.lab5.Server.stuff.HttpSender;
import ru.nsu.fit.cherniak.lab5.Server.stuff.SocketEvent;

/**
 * Запрос на сапуск кода
 */
public class HandRun implements SocketEvent {
    private JSContext ctx;

    public HandRun(JSContext ctx) {
        this.ctx = ctx;
    }

    @Override
    public void onSocket(HttpParser httpParser, HttpSender httpSender) {
        String hash = httpParser.getPathInfo().get("hash");
        JSOne one = ctx.getItem(hash);
        byte[] code;
        try {
            code = httpParser.getBody();
        } catch (ExceptionHttpParser exceptionHttpParser) {
            httpSender.sendError("Cannot get code", 502);
            return;
        }
        boolean res = one.start(code);
        if (res) {
            httpSender.sendData("Started");
        } else {
            httpSender.sendData("Cannot started\nHint: maybe you has another running code?");
        }
    }
}
