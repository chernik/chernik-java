package ru.nsu.fit.cherniak.lab5.JSPlugin.JSContext;

public interface JSInterruptRunnable {
    Object run() throws JSInterruptException;
}
