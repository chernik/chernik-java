package ru.nsu.fit.cherniak.lab5.JSPlugin.JSContext;

/**
 * Исключение, бросающееся при прерывании потока
 */
public class JSInterruptException extends InterruptedException {
}
