package ru.nsu.fit.cherniak.lab5.JSPlugin.JSContext;

import jdk.nashorn.api.scripting.ScriptObjectMirror;

/**
 * Интерфейс JS - Plugin
 */
public class JSIntro {
    /**
     * Максимальная глубина обхода дерева
     */
    private static final int DEEP = 100;
    private final Object sync;
    private StringBuilder sb;

    public JSIntro() {
        sb = new StringBuilder();
        sync = new Object();
    }

    /**
     * Проверка на прерванный поток
     *
     * @throws JSInterruptException < посмотри описание
     */
    private void check() throws JSInterruptException {
        if (Thread.interrupted()) {
            throw new JSInterruptException();
        }
    }

    /**
     * Сброс буффера print
     */
    public void resetPrintBuilder() {
        synchronized (sync) {
            sb = new StringBuilder();
        }
    }

    /**
     * @return буффер print
     */
    public String getPrintContent() {
        synchronized (sync) {
            return sb.toString();
        }
    }

    /**
     * JS метод
     * Печать в буффер
     *
     * @param str объект для печати
     * @throws InterruptedException < посмотри описание
     */
    public void print(Object str) throws InterruptedException {
        check();
        synchronized (sync) {
            sb.append(str);
        }
        check();
    }

    /**
     * JS метод
     * Возвращает древовидную информацию об объекте
     * <p>
     * Числа представляются объектом Number
     * Строки - String
     * Объекты, массивы, функции - ScriptObjectMirror
     *
     * @param o объект
     * @return строка
     * @throws JSInterruptException < см. описание
     */
    public String test(Object o) throws JSInterruptException {
        check();
        String res = testObject(o, 0).toString();
        check();
        return res;
    }

    /**
     * Рекурсивный обход объекта
     *
     * @param o   Объект
     * @param tab Текущая глубина
     * @return StringBuilder
     */
    private StringBuilder testObject(Object o, int tab) {
        if (o == null) { // на входе null
            return getTab(tab)
                    .append("NULL\n");
        }
        if (tab > DEEP) { // превышена глубина
            return getTab(tab)
                    .append("Stack limit\n");
        }
        if (o instanceof Number) { // Число
            return getTab(tab)
                    .append("{")
                    .append(o.toString())
                    .append("}\n");
        }
        if (o instanceof ScriptObjectMirror) { // JS Объект
            return testJSObject((ScriptObjectMirror) o, tab);
        }
        // Иначе выводим toString() и переходим к дереву классов
        return getTab(tab)
                .append("to string > ")
                .append(o.toString())
                .append("\n")
                .append(testClass(o.getClass(), tab));
    }

    /**
     * Рекурсивный обход JS объекта
     *
     * @param o   Объект
     * @param tab Текущая глубина
     * @return Строка
     */
    private StringBuilder testJSObject(ScriptObjectMirror o, int tab) {
        StringBuilder sb = getTab(tab)
                .append("JSObject > ")
                .append(o.isArray() ? "array" : "")
                .append(o.isFunction() ? "function" : "")
                .append(" > ")
                .append(o.toString()).append("\n");
        o.forEach((key, obj) -> { // Обход объекта
            sb.append(getTab(tab + 1))
                    .append(key)
                    .append(" -> \n")
                    .append(testObject(obj, tab + 2));
        });
        return sb;
    }

    /**
     * Обход иерархии классов
     *
     * @param cl  Класс
     * @param tab Текущая глубина
     * @return Строка
     */
    private StringBuilder testClass(Class<?> cl, int tab) {
        if (cl == null) { // Заглушка для null
            return new StringBuilder();
        }
        StringBuilder sb = getTab(tab)
                .append(cl.getName())
                .append("\n")
                .append(testClass(cl.getSuperclass(), tab + 1)); // Суперкласс всегда один (может быть null)
        Class<?> arr[] = cl.getInterfaces(); // Интерфейсов много
        for (Class<?> anArr : arr) { // Обходим
            sb.append(testClass(anArr, tab + 1));
        }
        return sb;
    }

    /**
     * Формирует отступ по глубине
     *
     * @param tab Глубина
     * @return ||||||||
     */
    private StringBuilder getTab(int tab) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < tab; i++) {
            sb.append("|");
        }
        return sb;
    }
}
