package ru.nsu.fit.cherniak.lab5.JSPlugin.JSComm;

import ru.nsu.fit.cherniak.lab5.JSPlugin.JSContext.JSContext;
import ru.nsu.fit.cherniak.lab5.MatherServer.EnderContainer;
import ru.nsu.fit.cherniak.lab5.MatherServer.Interceptors.UnorderedPipeLine;
import ru.nsu.fit.cherniak.lab5.MatherServer.MatherHttpServer;
import ru.nsu.fit.cherniak.lab5.MatherServer.Mathers.AnyMather;
import ru.nsu.fit.cherniak.lab5.MatherServer.Mathers.StringMather;
import ru.nsu.fit.cherniak.lab5.MatherServer.Plugins.HttpServerCloseablePlugin;
import ru.nsu.fit.cherniak.lab5.MatherServer.PointContainer;
import ru.nsu.fit.cherniak.lab5.MatherServer.ServerContext;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;

/**
 * Плагин для работы с JS на сервере
 */
public class JSPlugin implements HttpServerCloseablePlugin {
    private JSContext ctx;
    private byte[] htm;

    public JSPlugin() {
        InputStream file = JSPlugin.class.getClassLoader().getResourceAsStream("index.htm");
        if (file == null) {
            htm = "<h1>Open resource error</h1>".getBytes();
            return;
        }
        try {
            htm = file.readAllBytes();
        } catch (IOException e) {
            htm = "<h1>Load resource error</h1>".getBytes();
            return;
        }
        try {
            file.close();
        } catch (IOException ignored) {
        }
    }

    @Override
    public void initialize(ServerContext serverContext) {
        MatherHttpServer s = serverContext.getServer();
        ctx = new JSContext(1);
        s.register(Collections.singletonList(
                new StringMather("js", null)
        ), new EnderContainer(() -> new HandNew(ctx)));
        s.registerPoints(Arrays.asList(
                new PointContainer(
                        new StringMather("js", null)
                ),
                new PointContainer(
                        new AnyMather("hash"),
                        new UnorderedPipeLine(new HasHash(ctx))
                )
        ), new EnderContainer(() -> new HandGui(ctx, htm)));
        s.register(Arrays.asList(
                new StringMather("js", null),
                new AnyMather("hash"),
                new StringMather("state", null)
        ), new EnderContainer(() -> new HandState(ctx)));
        s.register(Arrays.asList(
                new StringMather("js", null),
                new AnyMather("hash"),
                new StringMather("run", null)
        ), new EnderContainer(() -> new HandRun(ctx)));
        s.register(Arrays.asList(
                new StringMather("js", null),
                new AnyMather("hash"),
                new StringMather("lastcode", null)
        ), new EnderContainer(() -> new HandCode(ctx)));
        s.register(Arrays.asList(
                new StringMather("js", null),
                new AnyMather("hash"),
                new StringMather("interrupt", null)
        ), new EnderContainer(() -> new HandKill(ctx)));
    }

    @Override
    public String getName() {
        return "JS Plugin";
    }

    @Override
    public void destroy() {
        ctx.destroy();
    }
}
