package ru.nsu.fit.cherniak.lab5.JSPlugin.JSComm;

import ru.nsu.fit.cherniak.lab5.JSPlugin.JSContext.JSContext;
import ru.nsu.fit.cherniak.lab5.Server.parse.HttpParser;
import ru.nsu.fit.cherniak.lab5.Server.stuff.HttpSender;
import ru.nsu.fit.cherniak.lab5.Server.stuff.SocketEvent;

/**
 * Создание нового JS контекста
 */
public class HandNew implements SocketEvent {
    private final int CODE_MOVED_PERMAMENTLY = 301;
    private JSContext ctx;

    public HandNew(JSContext ctx) {
        this.ctx = ctx;
    }

    @Override
    public void onSocket(HttpParser httpParser, HttpSender httpSender) {
        String hash = ctx.newItem();
        httpSender.setVar("Location", "/js/" + hash);
        httpSender.sendError("<a href=\"/js/" + hash + "\">Redirect</a>", CODE_MOVED_PERMAMENTLY);
    }
}
