var ctx = Context;
var obj = {};
obj.func = function(){print("kyky")}; // Function
obj.arr = [1,{},NaN]; // Array
obj.ctx = ctx; // Java object
obj.print = ctx.print; // Java method
obj.df = undefined; // Undefined

//ctx.print(obj.ctx.test(this));

var stk = function(i){
    if(i === undefined){
        return stk(0);
    }
    if(i === 10){
        while(true){
            ctx.print("hi");
        }
    }
    return stk(i+1);
};

//dfg(r);

stk();